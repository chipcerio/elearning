/*
 * Copyright (C) 2013 Alistair Cerio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eng.cucumber.elearning.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import eng.cucumber.elearning.util.DBUtils.AssignmentColumns;
import eng.cucumber.elearning.util.DBUtils.ClassGroupColumns;
import eng.cucumber.elearning.util.DBUtils.CourseCategoryColumns;
import eng.cucumber.elearning.util.DBUtils.CourseColumns;
import eng.cucumber.elearning.util.DBUtils.ECornerSpeakerColumns;
import eng.cucumber.elearning.util.DBUtils.ECornerVideoColumns;
import eng.cucumber.elearning.util.DBUtils.InstructorColumns;
import eng.cucumber.elearning.util.DBUtils.LessonColumns;
import eng.cucumber.elearning.util.DBUtils.LessonECornerColumns;
import eng.cucumber.elearning.util.DBUtils.LessonTedColumns;
import eng.cucumber.elearning.util.DBUtils.LessonVideoColumns;
import eng.cucumber.elearning.util.DBUtils.TedSpeakerColumns;
import eng.cucumber.elearning.util.DBUtils.TedTalksColumns;
import eng.cucumber.elearning.util.DBUtils.UniversityColumns;
import eng.cucumber.elearning.util.DBUtils.UpdatesColumns;
import eng.cucumber.elearning.util.DBUtils.UserColumns;
import eng.cucumber.elearning.util.DebugLog;

/* Database class */
public class ELearningDb {
    private static final String DATABASE_NAME = "elearning.db";
    private static final int DATABASE_VERSION = 1;

    private DbHelper mDbHelper;
    private final Context mContext;
    private SQLiteDatabase mSQLite;

    private static final String TAG = "ELearningDb";

    interface Tables {
        String INSTRUCTORS = "instructor";
        String COURSES = "course";
        String UNIVERSITIES = "university";
        String LESSONS = "lesson";
        String CLASS_GROUPS = "class_groups";
        String LESSON_VIDEOS = "lesson_videos";
        String TED_SPEAKERS = "ted_speakers";
        String TED_TALKS = "ted_talks";
        String ECORNER_SPEAKERS = "ecorner_speakers";
        String ECORNER_VIDEOS = "ecorner_video";
        String LESSONS_TED = "lesson_ted";
        String LESSONS_ECORNER = "lesson_ecorner";
        String ASSIGNMENTS = "assignment";
        String USERS = "users";
        String CATEGORIES = "category";
        String COURSE_CATEGORY = "course_category";
        String UPDATES = "updates";

        String COURSE_JOIN_INSTRUCTOR = "course INNER JOIN instructor ON course.course_instructor1=instructor.instructor_nid";
        String COURSE_JOIN_COURSE_CATEGORY = "course INNER JOIN course_category ON course.course_nid=course_category.course_id";
    }

    interface SubQuery {
        /*
        SELECT course.course_id, course.course_title, course.course_body, course. course_image,
               instructor.instructor_title, instructor.instructor_body, instructor.instructor_photo
        FROM  course
        INNER JOIN instructor
        ON  course.course_instructor1=instructor.instructor_nid
        WHERE course_id=4
         */
        String COURSE_OUTLINE =
                "SELECT " + Qualified.COURSE_ID + ", "
                        + Qualified.COURSE_TITLE + ", "
                        + Qualified.COURSE_BODY + ", "
                        + Qualified.COURSE_IMAGE + ", "
                        + Qualified.INSTRUCTOR_TITLE + ", "
                        + Qualified.INSTRUCTOR_BODY + ", "
                        + Qualified.INSTRUCTOR_IMAGE +
                        " FROM "  + Tables.COURSE_JOIN_INSTRUCTOR +
                        " WHERE " + CourseColumns.COURSE_ID +
                        "=?";

        /*
        SELECT course.course_title, course.course_image
        FROM course
        INNER JOIN course_category
        ON course.course_nid=course_category.course_id
        WHERE category_id=1
         */
        String SELECT_CATEGORY =
                "SELECT " + Qualified.COURSE_TITLE + ", "
                        + Qualified.COURSE_IMAGE +
                        " FROM "  + Tables.COURSE_JOIN_COURSE_CATEGORY +
                        " WHERE " + CourseCategoryColumns.CATEGORY_ID +
                        "=?";

        /*
        SELECT course.course_id, course.course_title, course.course_body, course. course_image,
               instructor.instructor_title, instructor.instructor_body, instructor.instructor_photo
        FROM  course
        INNER JOIN instructor
        ON  course.course_instructor1=instructor.instructor_nid
        WHERE course_title LIKE '%GMAT%'

        source:
        http://stackoverflow.com/a/5719869/1076574
         */
        String COURSE_OUTLINE_SEARCH_BY_TITLE =
                "SELECT " + Qualified.COURSE_ID + ", "
                        + Qualified.COURSE_TITLE + ", "
                        + Qualified.COURSE_BODY + ", "
                        + Qualified.COURSE_IMAGE + ", "
                        + Qualified.INSTRUCTOR_TITLE + ", "
                        + Qualified.INSTRUCTOR_BODY + ", "
                        + Qualified.INSTRUCTOR_IMAGE +
                        " FROM "  + Tables.COURSE_JOIN_INSTRUCTOR +
                        " WHERE " + CourseColumns.COURSE_TITLE +
                        " LIKE (?)";
    }

    interface Qualified {
        String COURSE_ID =    Tables.COURSES + "." + CourseColumns.COURSE_ID;
        String COURSE_TITLE = Tables.COURSES + "." + CourseColumns.COURSE_TITLE;
        String COURSE_BODY =  Tables.COURSES + "." + CourseColumns.COURSE_BODY;
        String COURSE_IMAGE = Tables.COURSES + "." + CourseColumns.COURSE_IMAGE;

        String INSTRUCTOR_TITLE = Tables.INSTRUCTORS + "." + InstructorColumns.INSTRUCTOR_TITLE;
        String INSTRUCTOR_BODY =  Tables.INSTRUCTORS + "." + InstructorColumns.INSTRUCTOR_BODY;
        String INSTRUCTOR_IMAGE = Tables.INSTRUCTORS + "." + InstructorColumns.INSTRUCTOR_IMAGE;


    }

    interface SelectQuery {
        String ALL_COURSES = "SELECT * FROM " + Tables.COURSES + ";";
        String COURSE_DETAIL= "SELECT * FROM " + Tables.COURSES + " WHERE " + CourseColumns.COURSE_ID + "=?";
        String ALL_UNIVERSITY = "SELECT * FROM " + Tables.UNIVERSITIES + ";";
        String UNIVERSITY_DETAIL= "SELECT * FROM " + Tables.UNIVERSITIES + " WHERE " + UniversityColumns.UNIVERSITY_ID + "=?";
        String ALL_INSTRUCTORS = "SELECT * FROM " + Tables.INSTRUCTORS + ";";
        String INSTRUCTOR_DETAIL ="SELECT * FROM " + Tables.INSTRUCTORS + " WHERE " + InstructorColumns.INSTRUCTOR_ID + "=?";

        String ALL_TED_TALKS =  "SELECT * FROM " + Tables.TED_TALKS;
        String TED_TALKS_DETAIL = ALL_TED_TALKS + " WHERE " + TedTalksColumns.TED_TALKS_ID + "=?";
        String TED_TALKS_IMAGE =  "SELECT * FROM " + Tables.TED_TALKS + " WHERE " + TedTalksColumns.TED_TALKS_ID + "=?";

        String ALL_ECORNER = "SELECT * FROM " + Tables.ECORNER_VIDEOS;
        String ECORNER_DETAIL = ALL_ECORNER + " WHERE " + ECornerVideoColumns.ECORNER_VIDEO_ID + "=?";
        String ECORNER_IMAGE = "SELECT * FROM " + Tables.ECORNER_VIDEOS + " WHERE " + ECornerVideoColumns.ECORNER_VIDEO_ID + "=?";

        String SEARCH_USER = "SELECT * FROM " + Tables.USERS + " WHERE " + UserColumns.USER_NAME + "=?";
        String ALL_USERS = "SELECT * FROM " + Tables.USERS + ";";

        String ALL_CATEGORIES = "SELECT * FROM " + Tables.CATEGORIES + ";";
        String CLASS_GROUP = "SELECT * FROM " + Tables.CLASS_GROUPS + " WHERE " + ClassGroupColumns.CLASS_GROUP_TITLE + "=?";

        String TED_VIDEO_GROUP = "SELECT * FROM " + Tables.TED_TALKS + " WHERE " + TedTalksColumns.TED_TALKS_GROUP + "=?";
        String STANFORD_VIDEO_GROUP = "SELECT * FROM " + Tables.ECORNER_VIDEOS + " WHERE " + ECornerVideoColumns.ECORNER_VIDEO_GROUP + "=?";

        String LAST_MODIFIED = "SELECT * FROM " + Tables.UPDATES + " WHERE " + UpdatesColumns.UPDATE_ID + "=(SELECT MAX(" + UpdatesColumns.UPDATE_ID + ") FROM " + Tables.UPDATES + ")";
    }

    public ELearningDb(Context context) {
        mContext = context;
    }

    public ELearningDb open() {
        mDbHelper = new DbHelper(mContext);
        mSQLite = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        mDbHelper.close();
    }

    /* should be called after open() */
    public long insertInstructorRow(int nid, int tid, String name, String body,String photo, String course) {
        ContentValues cv = new ContentValues();
        cv.put(InstructorColumns.INSTRUCTOR_NID, nid);
        cv.put(InstructorColumns.INSTRUCTOR_TID, tid);
        cv.put(InstructorColumns.INSTRUCTOR_TITLE, name);
        cv.put(InstructorColumns.INSTRUCTOR_BODY, body);
        cv.put(InstructorColumns.INSTRUCTOR_IMAGE, photo);
        cv.put(InstructorColumns.INSTRUCTOR_COURSES, course);

        DebugLog.d(TAG, "nid=" + nid + " inserted >>>>> " + Tables.INSTRUCTORS);
        return mSQLite.insert(Tables.INSTRUCTORS, null, cv);
    }

    public int updateInstructorRow(int nid, int tid, String name, String body,String photo, String course) {
        ContentValues cv = new ContentValues();
        cv.put(InstructorColumns.INSTRUCTOR_NID, nid);
        cv.put(InstructorColumns.INSTRUCTOR_TID, tid);
        cv.put(InstructorColumns.INSTRUCTOR_TITLE, name);
        cv.put(InstructorColumns.INSTRUCTOR_BODY, body);
        cv.put(InstructorColumns.INSTRUCTOR_IMAGE, photo);
        cv.put(InstructorColumns.INSTRUCTOR_COURSES, course);

        DebugLog.d(TAG, "nid=" + nid + " updated >>>>> " + Tables.INSTRUCTORS);
        return mSQLite.update(Tables.INSTRUCTORS, cv,
                InstructorColumns.INSTRUCTOR_NID + "=?",
                new String[] {Integer.toString(nid)});
    }

    public long insertCourseRow(String title, int nid, int tid, String body, String image,
            String prereq, String textbook, String faq, int univ1, int univ2, int cat1, int cat2,
            int instr1, int instr2, String duration, String session, int audience) {
        ContentValues cv = new ContentValues();
        cv.put(CourseColumns.COURSE_TITLE, title);
        cv.put(CourseColumns.COURSE_NID, nid);
        cv.put(CourseColumns.COURSE_TID, tid);
        cv.put(CourseColumns.COURSE_BODY, body);
        cv.put(CourseColumns.COURSE_IMAGE, image);
        cv.put(CourseColumns.COURSE_PREREQUISITES, prereq);
        cv.put(CourseColumns.COURSE_TEXTBOOK, textbook);
        cv.put(CourseColumns.COURSE_FAQ, faq);
        cv.put(CourseColumns.COURSE_UNIVERSITY1, univ1);
        cv.put(CourseColumns.COURSE_UNIVERSITY2, univ2);
        cv.put(CourseColumns.COURSE_CATEGORY1, cat1);
        cv.put(CourseColumns.COURSE_CATEGORY2, cat2);
        cv.put(CourseColumns.COURSE_INSTRUCTOR1, instr1);
        cv.put(CourseColumns.COURSE_INSTRUCTOR2, instr2);
        cv.put(CourseColumns.COURSE_DURATION, duration);
        cv.put(CourseColumns.COURSE_NEXTSESSION, session);
        cv.put(CourseColumns.COURSE_GROUP_AUDIENCE, audience);

        DebugLog.d(TAG, "nid=" + nid + " inserted >>>>> " + Tables.COURSES);
        return mSQLite.insert(Tables.COURSES, null, cv);
    }

    public int updateCourseRow(String title, int nid, int tid, String body, String image,
            String prereq, String textbook, String faq, int univ1, int univ2, int cat1, int cat2,
            int instr1, int instr2, String duration, String session, int audience) {
        ContentValues cv = new ContentValues();
        cv.put(CourseColumns.COURSE_TITLE, title);
        cv.put(CourseColumns.COURSE_NID, nid);
        cv.put(CourseColumns.COURSE_TID, tid);
        cv.put(CourseColumns.COURSE_BODY, body);
        cv.put(CourseColumns.COURSE_IMAGE, image);
        cv.put(CourseColumns.COURSE_PREREQUISITES, prereq);
        cv.put(CourseColumns.COURSE_TEXTBOOK, textbook);
        cv.put(CourseColumns.COURSE_FAQ, faq);
        cv.put(CourseColumns.COURSE_UNIVERSITY1, univ1);
        cv.put(CourseColumns.COURSE_UNIVERSITY2, univ2);
        cv.put(CourseColumns.COURSE_CATEGORY1, cat1);
        cv.put(CourseColumns.COURSE_CATEGORY2, cat2);
        cv.put(CourseColumns.COURSE_INSTRUCTOR1, instr1);
        cv.put(CourseColumns.COURSE_INSTRUCTOR2, instr2);
        cv.put(CourseColumns.COURSE_DURATION, duration);
        cv.put(CourseColumns.COURSE_NEXTSESSION, session);
        cv.put(CourseColumns.COURSE_GROUP_AUDIENCE, audience);

        DebugLog.d(TAG, "nid=" + nid + " updated >>>>> " + Tables.COURSES);
        return mSQLite.update(Tables.COURSES, cv,
                CourseColumns.COURSE_NID + "=?",
                new String[] {Integer.toString(nid)});
    }

    public long insertUniversityRow(int nid, int tid, String title, String body, String banner) {
        ContentValues cv = new ContentValues();
        cv.put(UniversityColumns.UNIVERSITY_NID, nid);
        cv.put(UniversityColumns.UNIVERSITY_TID, tid);
        cv.put(UniversityColumns.UNIVERSITY_TITLE, title);
        cv.put(UniversityColumns.UNIVERSITY_BODY, body);
        cv.put(UniversityColumns.UNIVERSITY_BANNER, banner);

        DebugLog.d(TAG, "nid=" + nid + " inserted >>>>> " + Tables.UNIVERSITIES);
        return mSQLite.insert(Tables.UNIVERSITIES, null, cv);
    }
    
    public long insertUpdatesRow(int id, String date) {
        ContentValues cv = new ContentValues();
        cv.put(UpdatesColumns.UPDATE_ID, id);
        cv.put(UpdatesColumns.UPDATE_LAST_MODIFIED, date);

        DebugLog.d(TAG, "id=" + id + " inserted >>>>> " + Tables.UPDATES);
        return mSQLite.insert(Tables.UPDATES, null, cv);
    }

    public int updateUniversityRow(int nid, int tid, String title, String body, String banner) {
        ContentValues cv = new ContentValues();
        cv.put(UniversityColumns.UNIVERSITY_NID, nid);
        cv.put(UniversityColumns.UNIVERSITY_TID, tid);
        cv.put(UniversityColumns.UNIVERSITY_TITLE, title);
        cv.put(UniversityColumns.UNIVERSITY_BODY, body);
        cv.put(UniversityColumns.UNIVERSITY_BANNER, banner);

        DebugLog.d(TAG, "nid=" + nid + " updated >>>>> " + Tables.UNIVERSITIES);
        return mSQLite.update(Tables.UNIVERSITIES, cv,
                UniversityColumns.UNIVERSITY_NID + "=?",
                new String[] {Integer.toString(nid)});
    }

    public long insertLessonRow(int nid, int tid, String title, String body) {
        ContentValues cv = new ContentValues();
        cv.put(LessonColumns.LESSON_NID, nid);
        cv.put(LessonColumns.LESSON_TID, tid);
        cv.put(LessonColumns.LESSON_TITLE, title);
        cv.put(LessonColumns.LESSON_BODY, body);

        DebugLog.d(TAG, "nid=" + nid + " inserted >>>>> " + Tables.LESSONS);
        return mSQLite.insert(Tables.LESSONS, null, cv);
    }

    public int updateLessonRow(int nid, int tid, String title, String body) {
        ContentValues cv = new ContentValues();
        cv.put(LessonColumns.LESSON_NID, nid);
        cv.put(LessonColumns.LESSON_TID, tid);
        cv.put(LessonColumns.LESSON_TITLE, title);
        cv.put(LessonColumns.LESSON_BODY, body);

        DebugLog.d(TAG, "nid=" + nid + " updated >>>>> " + Tables.LESSONS);
        return mSQLite.update(Tables.LESSONS, cv,
                LessonColumns.LESSON_NID + "=?",
                new String[] {Integer.toString(nid)});
    }

    public long insertLessonTedRow(int nid, int tid, String title, String body, int category) {
        ContentValues cv = new ContentValues();
        cv.put(LessonTedColumns.LESSON_TED_NID, nid);
        cv.put(LessonTedColumns.LESSON_TED_TID, tid);
        cv.put(LessonTedColumns.LESSON_TED_TITLE, title);
        cv.put(LessonTedColumns.LESSON_TED_BODY, body);
        cv.put(LessonTedColumns.LESSON_TED_CATEGORY, category);

        DebugLog.d(TAG, "nid=" + nid + " inserted >>>>> " + Tables.LESSONS_TED);
        return mSQLite.insert(Tables.LESSONS_TED, null, cv);
    }

    public int updateLessonTedRow(int nid, int tid, String title, String body, int category) {
        ContentValues cv = new ContentValues();
        cv.put(LessonTedColumns.LESSON_TED_NID, nid);
        cv.put(LessonTedColumns.LESSON_TED_TID, tid);
        cv.put(LessonTedColumns.LESSON_TED_TITLE, title);
        cv.put(LessonTedColumns.LESSON_TED_BODY, body);
        cv.put(LessonTedColumns.LESSON_TED_CATEGORY, category);

        DebugLog.d(TAG, "nid=" + nid + " updated >>>>> " + Tables.LESSONS_TED);
        return mSQLite.update(Tables.LESSONS_TED, cv,
                LessonTedColumns.LESSON_TED_NID + "=?",
                new String[] {Integer.toString(nid)});
    }

    public long insertLessonECornerRow(int nid, int tid, String title, String body, int category) {
        ContentValues cv = new ContentValues();
        cv.put(LessonECornerColumns.LESSON_ECORNER_NID, nid);
        cv.put(LessonECornerColumns.LESSON_ECORNER_TID, tid);
        cv.put(LessonECornerColumns.LESSON_ECORNER_TITLE, title);
        cv.put(LessonECornerColumns.LESSON_ECORNER_BODY, body);
        cv.put(LessonECornerColumns.LESSON_ECORNER_CATEGORY, category);

        DebugLog.d(TAG, "nid=" + nid + " inserted >>>>> " + Tables.LESSONS_ECORNER);
        return mSQLite.insert(Tables.LESSONS_ECORNER, null, cv);
    }

    public int updateLessonECornerRow(int nid, int tid, String title, String body, int category) {
        ContentValues cv = new ContentValues();
        cv.put(LessonECornerColumns.LESSON_ECORNER_NID, nid);
        cv.put(LessonECornerColumns.LESSON_ECORNER_TID, tid);
        cv.put(LessonECornerColumns.LESSON_ECORNER_TITLE, title);
        cv.put(LessonECornerColumns.LESSON_ECORNER_BODY, body);
        cv.put(LessonECornerColumns.LESSON_ECORNER_CATEGORY, category);

        DebugLog.d(TAG, "nid=" + nid + " updated >>>>> " + Tables.LESSONS_ECORNER);
        return mSQLite.update(Tables.LESSONS_ECORNER, cv,
                LessonECornerColumns.LESSON_ECORNER_NID + "=?",
                new String[] {Integer.toString(nid)});
    }

    public long insertLessonVideoRow(int nid, int tid, String title, String body, String videoUrl,
            String videoSrt) {
        ContentValues cv = new ContentValues();
        cv.put(LessonVideoColumns.LESSON_VIDEO_NID, nid);
        cv.put(LessonVideoColumns.LESSON_VIDEO_TID, tid);
        cv.put(LessonVideoColumns.LESSON_VIDEO_TITLE, title);
        cv.put(LessonVideoColumns.LESSON_VIDEO_BODY, body);
        cv.put(LessonVideoColumns.LESSON_VIDEO_URL, videoUrl);
        cv.put(LessonVideoColumns.LESSON_VIDEO_SRT, videoSrt);

        DebugLog.d(TAG, "nid=" + nid + " inserted >>>>> " + Tables.LESSON_VIDEOS);
        return mSQLite.insert(Tables.LESSON_VIDEOS, null, cv);
    }

    public int updateLessonVideoRow(int nid, int tid, String title, String body, String videoUrl,
            String videoSrt) {
        ContentValues cv = new ContentValues();
        cv.put(LessonVideoColumns.LESSON_VIDEO_NID, nid);
        cv.put(LessonVideoColumns.LESSON_VIDEO_TID, tid);
        cv.put(LessonVideoColumns.LESSON_VIDEO_TITLE, title);
        cv.put(LessonVideoColumns.LESSON_VIDEO_BODY, body);
        cv.put(LessonVideoColumns.LESSON_VIDEO_URL, videoUrl);
        cv.put(LessonVideoColumns.LESSON_VIDEO_SRT, videoSrt);

        DebugLog.d(TAG, "nid=" + nid + " updated >>>>> " + Tables.LESSON_VIDEOS);
        return mSQLite.update(Tables.LESSON_VIDEOS, cv,
                LessonVideoColumns.LESSON_VIDEO_NID + "=?",
                new String[] {Integer.toString(nid)});
    }

    public long insertClassGroupsRow(int nid, int tid, String title, String body) {
        ContentValues cv = new ContentValues();
        cv.put(ClassGroupColumns.CLASS_GROUP_NID, nid);
        cv.put(ClassGroupColumns.CLASS_GROUP_TID, tid);
        cv.put(ClassGroupColumns.CLASS_GROUP_TITLE, title);
        cv.put(ClassGroupColumns.CLASS_GROUP_BODY, body);

        return mSQLite.insert(Tables.CLASS_GROUPS, null, cv);
    }

    public int updateClassGroupsRow(int nid, int tid, String title, String body) {
        ContentValues cv = new ContentValues();
        cv.put(ClassGroupColumns.CLASS_GROUP_NID, nid);
        cv.put(ClassGroupColumns.CLASS_GROUP_TID, tid);
        cv.put(ClassGroupColumns.CLASS_GROUP_TITLE, title);
        cv.put(ClassGroupColumns.CLASS_GROUP_BODY, body);

        return mSQLite.update(Tables.CLASS_GROUPS, cv,
                ClassGroupColumns.CLASS_GROUP_NID + "=?",
                new String[] {Integer.toString(nid)});
    }

    public long insertTedSpeakerRow(int nid, int tid, String name, String body,
            String speakerTitle, String photo, String bio) {
        ContentValues cv = new ContentValues();
        cv.put(TedSpeakerColumns.TED_SPEAKER_NID, nid);
        cv.put(TedSpeakerColumns.TED_SPEAKER_TID, tid);
        cv.put(TedSpeakerColumns.TED_SPEAKER_NAME, name);
        cv.put(TedSpeakerColumns.TED_SPEAKER_BODY, body);
        cv.put(TedSpeakerColumns.TED_SPEAKER_TITLE, speakerTitle);
        cv.put(TedSpeakerColumns.TED_SPEAKER_PHOTO, photo);
        cv.put(TedSpeakerColumns.TED_SPEAKER_BIO, bio);

        DebugLog.d(TAG, "nid=" + nid + " inserted >>>>> " + Tables.TED_SPEAKERS);
        return mSQLite.insert(Tables.TED_SPEAKERS, null, cv);
    }

    public int updateTedSpeakerRow(int nid, int tid, String name, String body,
            String speakerTitle, String photo, String bio) {
        ContentValues cv = new ContentValues();
        cv.put(TedSpeakerColumns.TED_SPEAKER_NID, nid);
        cv.put(TedSpeakerColumns.TED_SPEAKER_TID, tid);
        cv.put(TedSpeakerColumns.TED_SPEAKER_NAME, name);
        cv.put(TedSpeakerColumns.TED_SPEAKER_BODY, body);
        cv.put(TedSpeakerColumns.TED_SPEAKER_TITLE, speakerTitle);
        cv.put(TedSpeakerColumns.TED_SPEAKER_PHOTO, photo);
        cv.put(TedSpeakerColumns.TED_SPEAKER_BIO, bio);

        DebugLog.d(TAG, "nid=" + nid + " updated >>>>> " + Tables.TED_SPEAKERS);
        return mSQLite.update(Tables.TED_SPEAKERS, cv,
                TedSpeakerColumns.TED_SPEAKER_NID + "=?",
                new String[] {Integer.toString(nid)});
    }

    public long insertTedTalksRow(int nid, int tid, String title, String body, int author1, int author2,
            String image, String video, String mp3, String mp3Local, String srt, String srtLocal,
            String srtCn, String srtCnLocal, String videoLd, String videoLdLocal, String videoSd,
            String videoSdLocal, String videoHd, String videoHdLocal, int category1, int category2) {
        ContentValues cv = new ContentValues();
        cv.put(TedTalksColumns.TED_TALKS_NID, nid);
        cv.put(TedTalksColumns.TED_TALKS_TID, tid);
        cv.put(TedTalksColumns.TED_TALKS_TITLE, title);
        cv.put(TedTalksColumns.TED_TALKS_BODY, body);
        cv.put(TedTalksColumns.TED_TALKS_INSTRUCTOR1, author1);
        cv.put(TedTalksColumns.TED_TALKS_INSTRUCTOR2, author2);
        cv.put(TedTalksColumns.TED_TALKS_IMAGE, image);
        cv.put(TedTalksColumns.TED_TALKS_VIDEO, video);
        cv.put(TedTalksColumns.TED_TALKS_MP3, mp3);
        cv.put(TedTalksColumns.TED_TALKS_MP3_LOCAL, mp3Local);
        cv.put(TedTalksColumns.TED_TALKS_SRT, srt);
        cv.put(TedTalksColumns.TED_TALKS_SRT_LOCAL, srtLocal);
        cv.put(TedTalksColumns.TED_TALKS_SRT_CN, srtCn);
        cv.put(TedTalksColumns.TED_TALKS_SRT_CN_LOCAL, srtCnLocal);
        cv.put(TedTalksColumns.TED_TALKS_VIDEO_LD, videoLd);
        cv.put(TedTalksColumns.TED_TALKS_VIDEO_LD_LOCAL, videoLdLocal);
        cv.put(TedTalksColumns.TED_TALKS_VIDEO_SD, videoSd);
        cv.put(TedTalksColumns.TED_TALKS_VIDEO_SD_LOCAL, videoSdLocal);
        cv.put(TedTalksColumns.TED_TALKS_VIDEO_HD, videoHd);
        cv.put(TedTalksColumns.TED_TALKS_VIDEO_HD_LOCAL, videoHdLocal);
        cv.put(TedTalksColumns.TED_TALKS_CATEGORY1, category1);
        cv.put(TedTalksColumns.TED_TALKS_CATEGORY2, category2);

        DebugLog.d(TAG, "nid=" + nid + " inserted >>>>> " + Tables.TED_TALKS);
        return mSQLite.insert(Tables.TED_TALKS, null, cv);
    }

    public int updateTedTalksRow(int nid, int tid, String title, String body, int author1, int author2,
            String image, String video, String mp3, String mp3Local, String srt, String srtLocal,
            String srtCn, String srtCnLocal, String videoLd, String videoLdLocal, String videoSd,
            String videoSdLocal, String videoHd, String videoHdLocal, int category1, int category2) {
        ContentValues cv = new ContentValues();
        cv.put(TedTalksColumns.TED_TALKS_NID, nid);
        cv.put(TedTalksColumns.TED_TALKS_TID, tid);
        cv.put(TedTalksColumns.TED_TALKS_TITLE, title);
        cv.put(TedTalksColumns.TED_TALKS_BODY, body);
        cv.put(TedTalksColumns.TED_TALKS_INSTRUCTOR1, author1);
        cv.put(TedTalksColumns.TED_TALKS_INSTRUCTOR2, author2);
        cv.put(TedTalksColumns.TED_TALKS_IMAGE, image);
        cv.put(TedTalksColumns.TED_TALKS_VIDEO, video);
        cv.put(TedTalksColumns.TED_TALKS_MP3, mp3);
        cv.put(TedTalksColumns.TED_TALKS_MP3_LOCAL, mp3Local);
        cv.put(TedTalksColumns.TED_TALKS_SRT, srt);
        cv.put(TedTalksColumns.TED_TALKS_SRT_LOCAL, srtLocal);
        cv.put(TedTalksColumns.TED_TALKS_SRT_CN, srtCn);
        cv.put(TedTalksColumns.TED_TALKS_SRT_CN_LOCAL, srtCnLocal);
        cv.put(TedTalksColumns.TED_TALKS_VIDEO_LD, videoLd);
        cv.put(TedTalksColumns.TED_TALKS_VIDEO_LD_LOCAL, videoLdLocal);
        cv.put(TedTalksColumns.TED_TALKS_VIDEO_SD, videoSd);
        cv.put(TedTalksColumns.TED_TALKS_VIDEO_SD_LOCAL, videoSdLocal);
        cv.put(TedTalksColumns.TED_TALKS_VIDEO_HD, videoHd);
        cv.put(TedTalksColumns.TED_TALKS_VIDEO_HD_LOCAL, videoHdLocal);
        cv.put(TedTalksColumns.TED_TALKS_CATEGORY1, category1);
        cv.put(TedTalksColumns.TED_TALKS_CATEGORY2, category2);

        DebugLog.d(TAG, "nid=" + nid + " updated >>>>> " + Tables.TED_TALKS);
        return mSQLite.update(Tables.TED_TALKS, cv,
                TedTalksColumns.TED_TALKS_NID + "=?",
                new String[] {Integer.toString(nid)});
    }

    public long insertECornerSpeakersRow(int nid, int tid, String title, String body, String image,
            String company, String www) {
        ContentValues cv = new ContentValues();
        cv.put(ECornerSpeakerColumns.ECORNER_SPEAKER_NID, nid);
        cv.put(ECornerSpeakerColumns.ECORNER_SPEAKER_TID, tid);
        cv.put(ECornerSpeakerColumns.ECORNER_SPEAKER_TITLE, title);
        cv.put(ECornerSpeakerColumns.ECORNER_SPEAKER_BODY, body);
        cv.put(ECornerSpeakerColumns.ECORNER_SPEAKER_IMAGE, image);
        cv.put(ECornerSpeakerColumns.ECORNER_SPEAKER_COMPANY, company);
        cv.put(ECornerSpeakerColumns.ECORNER_SPEAKER_WWW, www);

        DebugLog.d(TAG, "nid=" + nid + " inserted >>>>> " + Tables.ECORNER_SPEAKERS);
        return mSQLite.insert(Tables.ECORNER_SPEAKERS, null, cv);
    }

    public int updateECornerSpeakersRow(int nid, int tid, String title, String body, String image,
            String company, String www) {
        ContentValues cv = new ContentValues();
        cv.put(ECornerSpeakerColumns.ECORNER_SPEAKER_NID, nid);
        cv.put(ECornerSpeakerColumns.ECORNER_SPEAKER_TID, tid);
        cv.put(ECornerSpeakerColumns.ECORNER_SPEAKER_TITLE, title);
        cv.put(ECornerSpeakerColumns.ECORNER_SPEAKER_BODY, body);
        cv.put(ECornerSpeakerColumns.ECORNER_SPEAKER_IMAGE, image);
        cv.put(ECornerSpeakerColumns.ECORNER_SPEAKER_COMPANY, company);
        cv.put(ECornerSpeakerColumns.ECORNER_SPEAKER_WWW, www);

        DebugLog.d(TAG, "nid=" + nid + " updated >>>>> " + Tables.ECORNER_SPEAKERS);
        return mSQLite.update(Tables.ECORNER_SPEAKERS, cv,
                ECornerSpeakerColumns.ECORNER_SPEAKER_NID+ "=?",
                new String[] {Integer.toString(nid)});
    }

    public long insertECornerVideoTable(int nid, int tid, String title, String body, int author1,
            int author2, int category1, int category2, String image,String video, String videoLocal, String srt,
            String srtLocal, String srtCn, String srtCnLocal, String mp3, String mp3Local) {
        ContentValues cv = new ContentValues();
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_NID, nid);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_TID, tid);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_TITLE, title);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_BODY, body);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_INSTRUCTOR1, author1);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_INSTRUCTOR2, author2);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_CATEGORY1, category1);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_CATEGORY2, category2);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_IMAGE, image);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_VIDEO, video);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_LOCAL, videoLocal);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_SRT, srt);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_SRT_LOCAL, srtLocal);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_SRT_CN, srtCn);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_SRT_CN_LOCAL, srtCnLocal);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_MP3, mp3);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_MP3_LOCAL, mp3Local);

        DebugLog.d(TAG, "nid=" + nid + " inserted >>>>> " + Tables.ECORNER_VIDEOS);
        return mSQLite.insert(Tables.ECORNER_VIDEOS, null, cv);
    }

    public int updateECornerVideoTable(int nid, int tid, String title, String body, int author1,
            int author2, int category1, int category2, String image,String video, String videoLocal, String srt,
            String srtLocal, String srtCn, String srtCnLocal, String mp3, String mp3Local) {
        ContentValues cv = new ContentValues();
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_NID, nid);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_TID, tid);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_TITLE, title);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_BODY, body);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_INSTRUCTOR1, author1);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_INSTRUCTOR2, author2);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_CATEGORY1, category1);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_CATEGORY2, category2);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_IMAGE, image);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_VIDEO, video);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_LOCAL, videoLocal);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_SRT, srt);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_SRT_LOCAL, srtLocal);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_SRT_CN, srtCn);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_SRT_CN_LOCAL, srtCnLocal);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_MP3, mp3);
        cv.put(ECornerVideoColumns.ECORNER_VIDEO_MP3_LOCAL, mp3Local);

        DebugLog.d(TAG, "nid=" + nid + " updated >>>>> " + Tables.ECORNER_VIDEOS);
        return mSQLite.update(Tables.ECORNER_VIDEOS, cv,
                ECornerVideoColumns.ECORNER_VIDEO_NID + "=?",
                new String[] {Integer.toString(nid)});
    }

    public long insertUser(String user, String pass, String salt, String email) {
        ContentValues cv = new ContentValues();
        cv.put(UserColumns.USER_NAME, user);
        cv.put(UserColumns.USER_PASSWORD_HASH, pass);
        cv.put(UserColumns.USER_SALT, salt);
        cv.put(UserColumns.USER_EMAIL, email);

        DebugLog.d(TAG, "user=" + user + " inserted >>>>> " + Tables.USERS);
        return mSQLite.insert(Tables.USERS, null, cv);
    }

    public String getData() {
        String[] columns = new String[] {
                InstructorColumns.INSTRUCTOR_ID, InstructorColumns.INSTRUCTOR_TITLE,
                InstructorColumns.INSTRUCTOR_BODY };
        Cursor c = mSQLite.query(Tables.INSTRUCTORS, columns, null, null, null, null, null);
        String result = "";

        // set 3 ints
        int iRow = c.getColumnIndex(InstructorColumns.INSTRUCTOR_ID);
        int iName = c.getColumnIndex(InstructorColumns.INSTRUCTOR_TITLE);
        int iHot = c.getColumnIndex(InstructorColumns.INSTRUCTOR_BODY);

        // start cursor from the begining
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            result = result + c.getString(iRow) + " " + c.getString(iName) + " " + c.getString(iHot) + "\n";
        }

        return result;
    }

    /* Database helper class */
    private static class DbHelper extends SQLiteOpenHelper {

        public DbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL("CREATE TABLE " + Tables.INSTRUCTORS + " ("
                    + InstructorColumns.INSTRUCTOR_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + InstructorColumns.INSTRUCTOR_NID + " INTEGER,"
                    + InstructorColumns.INSTRUCTOR_TID + " INTEGER,"
                    + InstructorColumns.INSTRUCTOR_TITLE + " TEXT NOT NULL,"
                    + InstructorColumns.INSTRUCTOR_BODY + " TEXT NOT NULL,"
                    + InstructorColumns.INSTRUCTOR_IMAGE + " TEXT,"
                    + InstructorColumns.INSTRUCTOR_COURSES + " TEXT);");

            db.execSQL("CREATE TABLE " + Tables.COURSES + " ("
                    + CourseColumns.COURSE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + CourseColumns.COURSE_NID + " INTEGER,"
                    + CourseColumns.COURSE_TID + " INTEGER,"
                    + CourseColumns.COURSE_TITLE + " TEXT NOT NULL,"
                    + CourseColumns.COURSE_BODY + " TEXT NOT NULL,"
                    + CourseColumns.COURSE_IMAGE + " TEXT,"
                    + CourseColumns.COURSE_PREREQUISITES + " TEXT,"
                    + CourseColumns.COURSE_TEXTBOOK + " TEXT,"
                    + CourseColumns.COURSE_FAQ + " TEXT,"
                    + CourseColumns.COURSE_UNIVERSITY1 + " INTEGER,"
                    + CourseColumns.COURSE_UNIVERSITY2 + " INTEGER,"
                    + CourseColumns.COURSE_CATEGORY1 + " INTEGER,"
                    + CourseColumns.COURSE_CATEGORY2 + " INTEGER,"
                    + CourseColumns.COURSE_INSTRUCTOR1 + " INTEGER,"
                    + CourseColumns.COURSE_INSTRUCTOR2 + " INTEGER,"
                    + CourseColumns.COURSE_DURATION + " TEXT,"
                    + CourseColumns.COURSE_NEXTSESSION + " TEXT,"
                    + CourseColumns.COURSE_GROUP_AUDIENCE + " INTEGER);");

            db.execSQL("CREATE TABLE " + Tables.UNIVERSITIES + " ("
                    + UniversityColumns.UNIVERSITY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + UniversityColumns.UNIVERSITY_NID + " INTEGER,"
                    + UniversityColumns.UNIVERSITY_TID + " INTEGER,"
                    + UniversityColumns.UNIVERSITY_TITLE + " TEXT NOT NULL,"
                    + UniversityColumns.UNIVERSITY_BODY + " TEXT NOT NULL,"
                    + UniversityColumns.UNIVERSITY_BANNER + " TEXT);");

            db.execSQL("CREATE TABLE " + Tables.LESSONS + " ("
                    + LessonColumns.LESSON_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + LessonColumns.LESSON_NID + " INTEGER,"
                    + LessonColumns.LESSON_TID + " INTEGER,"
                    + LessonColumns.LESSON_TITLE + " TEXT NOT NULL,"
                    + LessonColumns.LESSON_BODY + " TEXT NOT NULL);");

            db.execSQL("CREATE TABLE " + Tables.CLASS_GROUPS + " ("
                    + ClassGroupColumns.CLASS_GROUP_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + ClassGroupColumns.CLASS_GROUP_NID + " INTEGER,"
                    + ClassGroupColumns.CLASS_GROUP_TID + " INTEGER,"
                    + ClassGroupColumns.CLASS_GROUP_TITLE + " TEXT NOT NULL,"
                    + ClassGroupColumns.CLASS_GROUP_BODY + " TEXT NOT NULL);");

            db.execSQL("CREATE TABLE " + Tables.LESSON_VIDEOS + " ("
                    + LessonVideoColumns.LESSON_VIDEO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + LessonVideoColumns.LESSON_VIDEO_NID + " INTEGER,"
                    + LessonVideoColumns.LESSON_VIDEO_TID + " INTEGER,"
                    + LessonVideoColumns.LESSON_VIDEO_TITLE + " TEXT NOT NULL,"
                    + LessonVideoColumns.LESSON_VIDEO_BODY + " TEXT NOT NULL,"
                    + LessonVideoColumns.LESSON_VIDEO_URL + " TEXT,"
                    + LessonVideoColumns.LESSON_VIDEO_SRT + " TEXT);");

            db.execSQL("CREATE TABLE " + Tables.TED_SPEAKERS + " ("
                    + TedSpeakerColumns.TED_SPEAKER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + TedSpeakerColumns.TED_SPEAKER_NID + " INTEGER,"
                    + TedSpeakerColumns.TED_SPEAKER_TID + " INTEGER,"
                    + TedSpeakerColumns.TED_SPEAKER_NAME + " TEXT NOT NULL,"
                    + TedSpeakerColumns.TED_SPEAKER_BODY + " TEXT NOT NULL,"
                    + TedSpeakerColumns.TED_SPEAKER_TITLE + " TEXT,"
                    + TedSpeakerColumns.TED_SPEAKER_PHOTO + " TEXT,"
                    + TedSpeakerColumns.TED_SPEAKER_BIO + " TEXT);");

            db.execSQL("CREATE TABLE " + Tables.TED_TALKS + " ("
                    + TedTalksColumns.TED_TALKS_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + TedTalksColumns.TED_TALKS_NID + " INTEGER,"
                    + TedTalksColumns.TED_TALKS_TID + " INTEGER,"
                    + TedTalksColumns.TED_TALKS_TITLE + " TEXT NOT NULL,"
                    + TedTalksColumns.TED_TALKS_BODY + " TEXT NOT NULL,"
                    + TedTalksColumns.TED_TALKS_INSTRUCTOR1 + " INTEGER,"
                    + TedTalksColumns.TED_TALKS_INSTRUCTOR2 + " INTEGER,"
                    + TedTalksColumns.TED_TALKS_IMAGE + " TEXT,"
                    + TedTalksColumns.TED_TALKS_VIDEO + " TEXT,"
                    + TedTalksColumns.TED_TALKS_MP3 + " TEXT,"
                    + TedTalksColumns.TED_TALKS_MP3_LOCAL + " TEXT,"
                    + TedTalksColumns.TED_TALKS_SRT + " TEXT,"
                    + TedTalksColumns.TED_TALKS_SRT_LOCAL + " TEXT,"
                    + TedTalksColumns.TED_TALKS_SRT_CN + " TEXT,"
                    + TedTalksColumns.TED_TALKS_SRT_CN_LOCAL + " TEXT,"
                    + TedTalksColumns.TED_TALKS_VIDEO_LD + " TEXT,"
                    + TedTalksColumns.TED_TALKS_VIDEO_LD_LOCAL + " TEXT,"
                    + TedTalksColumns.TED_TALKS_VIDEO_SD + " TEXT,"
                    + TedTalksColumns.TED_TALKS_VIDEO_SD_LOCAL + " TEXT,"
                    + TedTalksColumns.TED_TALKS_VIDEO_HD + " TEXT,"
                    + TedTalksColumns.TED_TALKS_VIDEO_HD_LOCAL + " TEXT,"
                    + TedTalksColumns.TED_TALKS_CATEGORY1 + " INTEGER,"
                    + TedTalksColumns.TED_TALKS_CATEGORY2 + " INTEGER);");

            db.execSQL("CREATE TABLE " + Tables.ECORNER_SPEAKERS + " ("
                    + ECornerSpeakerColumns.ECORNER_SPEAKER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + ECornerSpeakerColumns.ECORNER_SPEAKER_NID + " INTEGER,"
                    + ECornerSpeakerColumns.ECORNER_SPEAKER_TID + " INTEGER,"
                    + ECornerSpeakerColumns.ECORNER_SPEAKER_TITLE + " TEXT NOT NULL,"
                    + ECornerSpeakerColumns.ECORNER_SPEAKER_BODY + " TEXT NOT NULL,"
                    + ECornerSpeakerColumns.ECORNER_SPEAKER_IMAGE + " TEXT,"
                    + ECornerSpeakerColumns.ECORNER_SPEAKER_COMPANY + " TEXT,"
                    + ECornerSpeakerColumns.ECORNER_SPEAKER_WWW + " TEXT);");

            db.execSQL("CREATE TABLE " + Tables.ECORNER_VIDEOS + " ("
                    + ECornerVideoColumns.ECORNER_VIDEO_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + ECornerVideoColumns.ECORNER_VIDEO_NID + " INTEGER,"
                    + ECornerVideoColumns.ECORNER_VIDEO_TID + " INTEGER,"
                    + ECornerVideoColumns.ECORNER_VIDEO_TITLE + " TEXT NOT NULL,"
                    + ECornerVideoColumns.ECORNER_VIDEO_BODY + " TEXT NOT NULL,"
                    + ECornerVideoColumns.ECORNER_VIDEO_INSTRUCTOR1 + " INTEGER,"
                    + ECornerVideoColumns.ECORNER_VIDEO_INSTRUCTOR2 + " INTEGER,"
                    + ECornerVideoColumns.ECORNER_VIDEO_IMAGE + " TEXT,"
                    + ECornerVideoColumns.ECORNER_VIDEO_CATEGORY1 + " INTEGER,"
                    + ECornerVideoColumns.ECORNER_VIDEO_CATEGORY2 + " INTEGER,"
                    + ECornerVideoColumns.ECORNER_VIDEO_VIDEO + " TEXT,"
                    + ECornerVideoColumns.ECORNER_VIDEO_LOCAL + " TEXT,"
                    + ECornerVideoColumns.ECORNER_VIDEO_SRT + " TEXT,"
                    + ECornerVideoColumns.ECORNER_VIDEO_SRT_LOCAL + " TEXT,"
                    + ECornerVideoColumns.ECORNER_VIDEO_SRT_CN + " TEXT,"
                    + ECornerVideoColumns.ECORNER_VIDEO_SRT_CN_LOCAL + " TEXT,"
                    + ECornerVideoColumns.ECORNER_VIDEO_MP3 + " TEXT,"
                    + ECornerVideoColumns.ECORNER_VIDEO_MP3_LOCAL + " TEXT);");

            db.execSQL("CREATE TABLE " + Tables.LESSONS_TED + " ("
                    + LessonTedColumns.LESSON_TED_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + LessonTedColumns.LESSON_TED_NID + " INTEGER,"
                    + LessonTedColumns.LESSON_TED_TID + " INTEGER,"
                    + LessonTedColumns.LESSON_TED_TITLE + " TEXT NOT NULL,"
                    + LessonTedColumns.LESSON_TED_BODY + " TEXT NOT NULL,"
                    + LessonTedColumns.LESSON_TED_CATEGORY + " TEXT);");

            db.execSQL("CREATE TABLE " + Tables.LESSONS_ECORNER + " ("
                    + LessonECornerColumns.LESSON_ECORNER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + LessonECornerColumns.LESSON_ECORNER_NID + " INTEGER,"
                    + LessonECornerColumns.LESSON_ECORNER_TID + " INTEGER,"
                    + LessonECornerColumns.LESSON_ECORNER_TITLE + " TEXT NOT NULL,"
                    + LessonECornerColumns.LESSON_ECORNER_BODY + " TEXT NOT NULL,"
                    + LessonECornerColumns.LESSON_ECORNER_CATEGORY + " TEXT);");

            db.execSQL("CREATE TABLE " + Tables.ASSIGNMENTS + " ("
                    + AssignmentColumns.ASSIGNMENT_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + AssignmentColumns.ASSIGNMENT_NID + " INTEGER,"
                    + AssignmentColumns.ASSIGNMENT_TID + " INTEGER,"
                    + AssignmentColumns.ASSIGNMENT_TITLE + " TEXT NOT NULL,"
                    + AssignmentColumns.ASSIGNMENT_BODY + " TEXT NOT NULL);");

            db.execSQL("CREATE TABLE " + Tables.USERS + " ("
                    + UserColumns.USER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                    + UserColumns.USER_NAME + " TEXT NOT NULL,"
                    + UserColumns.USER_PASSWORD_HASH + " TEXT NOT NULL,"
                    + UserColumns.USER_SALT + " TEXT NOT NULL,"
                    + UserColumns.USER_EMAIL + " TEXT NOT NULL,"
                    + " UNIQUE (" + UserColumns.USER_NAME + "));");

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXIST " + Tables.INSTRUCTORS);
            // TODO drop other tables
            onCreate(db);
        }

    }
}
