package eng.cucumber.elearning.database;
/*
 * Copyright (C) 2013 Alistair Cerio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.widget.Toast;
import eng.cucumber.elearning.database.ELearningDb.SelectQuery;
import eng.cucumber.elearning.database.ELearningDb.SubQuery;
import eng.cucumber.elearning.database.ELearningDb.Tables;
import eng.cucumber.elearning.util.DBUtils.CategoryColumns;
import eng.cucumber.elearning.util.DBUtils.ClassGroupColumns;
import eng.cucumber.elearning.util.DBUtils.CourseColumns;
import eng.cucumber.elearning.util.DBUtils.ECornerSpeakerColumns;
import eng.cucumber.elearning.util.DBUtils.ECornerVideoColumns;
import eng.cucumber.elearning.util.DBUtils.InstructorColumns;
import eng.cucumber.elearning.util.DBUtils.LessonColumns;
import eng.cucumber.elearning.util.DBUtils.LessonECornerColumns;
import eng.cucumber.elearning.util.DBUtils.LessonTedColumns;
import eng.cucumber.elearning.util.DBUtils.LessonVideoColumns;
import eng.cucumber.elearning.util.DBUtils.TedSpeakerColumns;
import eng.cucumber.elearning.util.DBUtils.TedTalksColumns;
import eng.cucumber.elearning.util.DBUtils.UniversityColumns;
import eng.cucumber.elearning.util.DBUtils.UpdatesColumns;
import eng.cucumber.elearning.util.DBUtils.UserColumns;
import eng.cucumber.elearning.util.DebugLog;

public class ELearningData {
    private static final String DATABASE_NAME = "elearning.db";
    private static final int DATABASE_VERSION = 1;

    private SQLiteDatabase mSqliteDb;
    private Context mContext;
    @SuppressWarnings("unused")
    private DbHelper mDbHelper;
    private String mDbPath;

    private static final String TAG = "ELearningData";

    public ELearningData(Context context) {
        mContext = context;
        mDbHelper = new DbHelper(mContext);
    }

    public List<String> getInstructors() {
        List<String> list = new ArrayList<String>();
        String name = "";

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.ALL_INSTRUCTORS, null);
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    name = cur.getString(cur.getColumnIndex(InstructorColumns.INSTRUCTOR_TITLE));
                    list.add(name);
                } while (cur.moveToNext());
            }
            cur.close();
        }
        return list;
    }

    public String getInstructorDetail(int position) {
        String detail = "";
        String pos = Integer.toString(position);

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.INSTRUCTOR_DETAIL, new String[] {pos});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    detail = cur.getString(cur.getColumnIndex(InstructorColumns.INSTRUCTOR_BODY));
                } while (cur.moveToNext());
            }
            cur.close();
        }
        return detail;
    }

    public String getInstructorImage(int position) {
        String image = "";
        String pos = Integer.toString(position);

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.INSTRUCTOR_DETAIL, new String[] {pos});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    image = cur.getString(cur.getColumnIndex(InstructorColumns.INSTRUCTOR_IMAGE));
                } while (cur.moveToNext());
            }
            cur.close();
        }

        return image;
    }

    public String getLastModifiedDate() {
        String date = "";

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.LAST_MODIFIED, null);
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    date = cur.getString(cur.getColumnIndex(UpdatesColumns.UPDATE_LAST_MODIFIED));
                } while (cur.moveToNext());
            }
            cur.close();
        }

        return date;
    }
    
    public int getIdOnLastModifiedDate() {
        String id = "";

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.LAST_MODIFIED, null);
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    id = cur.getString(cur.getColumnIndex(UpdatesColumns.UPDATE_ID));
                } while (cur.moveToNext());
            }
            cur.close();
        }

        return Integer.parseInt(id);
    }

    public List<String> getUniversities() {
        List<String> list = new ArrayList<String>();
        String name = "";

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.ALL_UNIVERSITY, null);
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    name = cur.getString(cur.getColumnIndex(UniversityColumns.UNIVERSITY_TITLE));
                    list.add(name);
                } while (cur.moveToNext());
            }
            cur.close();
        }
        return list;
    }

    public String getUniversityDetail(int position) {
        String name = "";
        String pos = Integer.toString(position);

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.UNIVERSITY_DETAIL, new String[] {pos});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    name = cur.getString(cur.getColumnIndex(UniversityColumns.UNIVERSITY_BODY));
                } while (cur.moveToNext());
            }
            cur.close();
        }
        return name;
    }

    public boolean hasDb() {
        if (mSqliteDb == null) {
            return false;
        } else {
            return true;
        }
    }

    public int getTedTalksGroupSize(String group) {
        Cursor cur = mSqliteDb.rawQuery(SelectQuery.TED_VIDEO_GROUP, new String[] {group});
        if (cur != null) {
            return cur.getCount();
        } else {
            return 0;
        }
    }

    public int getStanfordGroupSize(String group) {
        Cursor cur = mSqliteDb.rawQuery(SelectQuery.STANFORD_VIDEO_GROUP, new String[] {group});
        if (cur != null) {
            return cur.getCount();
        } else {
            return 0;
        }
    }

    /**
     * <p>This method returns a list of records by group. </p>
     * 
     * Valid columns for <b>columnName</b> are: </br>
     * <ul>
     * <li>{@code TedTalksColumns.TED_TALKS_IMAGE}</li>
     * </ul>
     * 
     * @param group       the String to be searched
     * @param columnName  column from TedTalksColumns interface
     * @return the String from columnName
     */
    public List<String> getTedTalksGroup(String group, String columnName) {
        List<String> list = new ArrayList<String>();
        String image = "";

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.TED_VIDEO_GROUP, new String[] {group});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    image = cur.getString(cur.getColumnIndex(columnName));
                    list.add(image);
                } while (cur.moveToNext());
            }
            cur.close();
        }

        return list;
    }

    /**
     * <p>This method returns a list of records by group. </p>
     * 
     * Valid columns for <b>columnName</b> are: </br>
     * <ul>
     * <li>{@code ECornerVideoColumns.ECORNER_VIDEO_IMAGE}</li>
     * </ul>
     * 
     * @param group       the String to be searched
     * @param columnName  column from TedTalksColumns interface
     * @return the String from columnName
     */
    public List<String> getStanfordGroup(String group, String columnName) {
        List<String> list = new ArrayList<String>();
        String image = "";

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.STANFORD_VIDEO_GROUP, new String[] {group});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    image = cur.getString(cur.getColumnIndex(columnName));
                    list.add(image);
                } while (cur.moveToNext());
            }
            cur.close();
        }

        return list;
    }

    public List<String> getCategories() {
        List<String> list = new ArrayList<String>();
        String cat = "";

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.ALL_CATEGORIES, null);
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    cat = cur.getString(cur.getColumnIndex(CategoryColumns.CATEGORY_TITLE));
                    list.add(cat);
                } while (cur.moveToNext());
            }
            cur.close();
        }

        return list;
    }

    /*
     * When Society, Networks, and Information is selected
     * or Mathematics and Statistics is selected
     * and etc
     */
    public List<String> getSubCategoryTitles(int position) {
        List<String> list = new ArrayList<String>();
        String title = "";
        String index = Integer.toString(position);

        Cursor cur = mSqliteDb.rawQuery(SubQuery.SELECT_CATEGORY, new String[] {index});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    title = cur.getString(cur.getColumnIndex(CourseColumns.COURSE_TITLE));
                    list.add(title);
                } while (cur.moveToNext());
            }
            cur.close();
        }

        return list;
    }

    /*
     * When Society, Networks, and Information is selected
     * or Mathematics and Statistics is selected
     * and etc
     */
    public List<String> getSubCategoryImages(int position) {
        List<String> list = new ArrayList<String>();
        String image = "";
        String index = Integer.toString(position);

        Cursor cur = mSqliteDb.rawQuery(SubQuery.SELECT_CATEGORY, new String[] {index});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    image = cur.getString(cur.getColumnIndex(CourseColumns.COURSE_IMAGE));
                    list.add(image);
                    DebugLog.d(ELearningData.class.getName(), "image=" + image);
                } while (cur.moveToNext());
            }
            cur.close();
        }

        return list;
    }

    public List<String> getCourses() {
        List<String> list = new ArrayList<String>();
        String name = "";

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.ALL_COURSES, null);
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    name = cur.getString(cur.getColumnIndex(CourseColumns.COURSE_TITLE));
                    list.add(name);
                } while (cur.moveToNext());
            }
            cur.close();
        }

        return list;
    }

    public String getCourseTitle(int position) {
        String title = "";
        String pos = Integer.toString(position);

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.COURSE_DETAIL, new String[] {pos});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    title = cur.getString(cur.getColumnIndex(CourseColumns.COURSE_TITLE));
                } while (cur.moveToNext());
            }
            cur.close();
        }
        return title;
    }

    public String getCourseImage(int position) {
        String image = "";
        String pos = Integer.toString(position);

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.COURSE_DETAIL, new String[] {pos});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    image = cur.getString(cur.getColumnIndex(CourseColumns.COURSE_IMAGE));
                } while (cur.moveToNext());
            }
            cur.close();
        }

        return image;
    }

    public String getCourseDetail(int position) {
        String body = "";
        String pos = Integer.toString(position);

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.COURSE_DETAIL, new String[] {pos});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    body = cur.getString(cur.getColumnIndex(CourseColumns.COURSE_BODY));
                } while (cur.moveToNext());
            }
            cur.close();
        }
        return body;
    }

    /*
     * getCourseOutline method requires 2 tables to display related data.  These are Course table
     * and the Instructor table
     */
    public String getCourseOutline(int position, String key) {
        String text = "";
        String pos = Integer.toString(position);

        Cursor cur = mSqliteDb.rawQuery(SubQuery.COURSE_OUTLINE, new String[] {pos});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    for (int i = 0; i < cur.getColumnCount(); i++) {
                        if (key.contentEquals(cur.getColumnName(i))) {
                            if (i == 0) text = cur.getString(i);
                            else text = cur.getString(i);
                        }
                    }

                } while (cur.moveToNext());
            }
            cur.close();
        }
        return text;
    }

    /**
     * <p>This method requires 2 tables to display related data.  These are Course table
     * and the Instructor table. </p>
     * 
     * Valid columns for <b>whichColumn</b> are: </br>
     * <ul>
     * <li>{@code CourseColumns.COURSE_TITLE}</li>
     * <li>{@code CourseColumns.COURSE_BODY}</li>
     * <li>{@code CourseColumns.COURSE_IMAGE}</li>
     * <li>{@code InstructorColumns.INSTRUCTOR_TITLE}</li>
     * <li>{@code InstructorColumns.INSTRUCTOR_BODY}</li>
     * <li>{@code InstructorColumns.INSTRUCTOR_IMAGE}</li>
     * </ul>
     * 
     * @param title       the String to be searched
     * @param whichColumn column from INNER JOIN between Course and Instructor
     * @return the String value from whichColumn
     */
    public String getCourseOutlineByTitle(String title, String whichColumn) {
        title = "%" + title + "%";
        String text = "";

        Cursor cur = mSqliteDb.rawQuery(SubQuery.COURSE_OUTLINE_SEARCH_BY_TITLE, new String[] {title});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    text = cur.getString(cur.getColumnIndex(whichColumn));
                } while (cur.moveToNext());
            }
            cur.close();
        }

        return text;
    }

    /**
     * <p>This method requires 2 tables to display related data. </p>
     * 
     * Valid columns for <b>columnName</b> are: </br>
     * <ul>
     * <li>{@code ClassGroupColumns.CLASS_GROUP_TITLE}</li>
     * <li>{@code ClassGroupColumns.CLASS_GROUP_BODY}</li>
     * </ul>
     * 
     * @param title       the String to be searched
     * @param columnName  column from ClassGroupColumns interface
     * @return the String from columnName
     */
    public String getClassGroup(String title, String columnName) {
        String text = "";

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.CLASS_GROUP, new String[] {title});
        if (cur != null) {
            if (cur.moveToFirst()) {
                text = cur.getString(cur.getColumnIndex(columnName));
            }
            cur.close();
        }

        return text;
    }

    public List<String> getTedTalks() {
        List<String> list = new ArrayList<String>();
        String title = "";

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.ALL_TED_TALKS, null);
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    title = cur.getString(cur.getColumnIndex(TedTalksColumns.TED_TALKS_TITLE));
                    list.add(title);
                } while (cur.moveToNext());
            }
            cur.close();
        }

        return list;
    }

    public String getTedTalksImage(int position) {
        String image = "";
        String pos = Integer.toString(position);

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.TED_TALKS_IMAGE, new String[] {pos});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    image = cur.getString(cur.getColumnIndex(TedTalksColumns.TED_TALKS_IMAGE));
                } while (cur.moveToNext());
            }
            cur.close();
        }

        return image;
    }

    public String getTedTalksTitle(int position) {
        String title = "";
        String pos = Integer.toString(position);
    
        Cursor cur = mSqliteDb.rawQuery(SelectQuery.TED_TALKS_DETAIL, new String[] {pos});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    title = cur.getString(cur.getColumnIndex(TedTalksColumns.TED_TALKS_TITLE));
                } while (cur.moveToNext());
            }
            cur.close();
        }
        return title;
    }

    public String getTedTalksDetails(int position) {
        String body = "";
        String pos = Integer.toString(position);
    
        Cursor cur = mSqliteDb.rawQuery(SelectQuery.TED_TALKS_DETAIL, new String[] {pos});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    body = cur.getString(cur.getColumnIndex(TedTalksColumns.TED_TALKS_BODY));
                } while (cur.moveToNext());
            }
            cur.close();
        }
        return body;
    }

    public String getTedSdVideoUrl(int position) {
        String video = "";
        String pos = Integer.toString(position);
    
        Cursor cur = mSqliteDb.rawQuery(SelectQuery.TED_TALKS_DETAIL, new String[] {pos});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    video = cur.getString(cur.getColumnIndex(TedTalksColumns.TED_TALKS_VIDEO_SD_LOCAL));
                } while (cur.moveToNext());
            }
            cur.close();
        }
        // http://site.com/dir/dir/dir/filename.mp4
        return video;
    }

    public String getTedSdVideoForLocal(int position) {
        String video = "";
        String pos = Integer.toString(position);
    
        Cursor cur = mSqliteDb.rawQuery(SelectQuery.TED_TALKS_DETAIL, new String[] {pos});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    video = cur.getString(cur.getColumnIndex(TedTalksColumns.TED_TALKS_VIDEO_SD_LOCAL));
                } while (cur.moveToNext());
            }
            cur.close();
        }
    
        if (video.contentEquals("")) {
            return video = "dummy.mp4";
        } else {
            //            String[] arr = video.split("/");
            //            video = arr[arr.length - 1].trim();
            return video;
        }
    }

    public String getECornerImage(int position) {
        String image = "";
        String pos = Integer.toString(position);

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.ECORNER_IMAGE, new String[] {pos});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    image = cur.getString(cur.getColumnIndex(ECornerVideoColumns.ECORNER_VIDEO_IMAGE));
                } while (cur.moveToNext());
            }
            cur.close();
        }

        return image;
    }

    public String getECornerTitle(int position) {
        String title = "";
        String pos = Integer.toString(position);

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.ECORNER_DETAIL, new String[] {pos});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    title = cur.getString(cur.getColumnIndex(ECornerVideoColumns.ECORNER_VIDEO_TITLE));
                } while (cur.moveToNext());
            }
            cur.close();
        }
        return title;
    }

    public String getECornerDetails(int position) {
        String body = "";
        String pos = Integer.toString(position);

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.ECORNER_DETAIL, new String[] {pos});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    body = cur.getString(cur.getColumnIndex(ECornerVideoColumns.ECORNER_VIDEO_BODY));
                } while (cur.moveToNext());
            }
            cur.close();
        }
        return body;
    }

    public String getECornerSdVideoUrl(int position) {
        String video = "";
        String pos = Integer.toString(position);

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.ECORNER_DETAIL, new String[] {pos});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    video = cur.getString(cur.getColumnIndex(ECornerVideoColumns.ECORNER_VIDEO_VIDEO));
                } while (cur.moveToNext());
            }
            cur.close();
        }
        // http://site.com/dir/dir/dir/filename.mp4
        return video;
    }

    public String getECornerSdVideoForLocal(int position) {
        String video = "";
        String pos = Integer.toString(position);

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.ECORNER_DETAIL, new String[] {pos});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    video = cur.getString(cur.getColumnIndex(ECornerVideoColumns.ECORNER_VIDEO_LOCAL));
                } while (cur.moveToNext());
            }
            cur.close();
        }

        if (video.contentEquals("")) {
            return video = "dummy.mp4";
        } else {
            //            String[] arr = video.split("/");
            //            video = arr[arr.length - 1].trim();
            return video;
        }
    }

    public String getSalt(String user) {
        String salt = "";

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.SEARCH_USER, new String[] {user});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    salt = cur.getString(cur.getColumnIndex(UserColumns.USER_SALT));
                } while (cur.moveToNext());
            }
            cur.close();
        }
        return salt;
    }

    public String getHashedPassword(String user) {
        String hash = "";

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.SEARCH_USER, new String[] {user});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    hash = cur.getString(cur.getColumnIndex(UserColumns.USER_PASSWORD_HASH));
                } while (cur.moveToNext());
            }
            cur.close();
        }
        return hash;
    }

    /**
     * <p>This method returns a list of records by group. </p>
     * 
     * Valid columns for <b>columnName</b> are: </br>
     * <ul>
     * <li>{@code UserColumns.USER_NAME}</li>
     * <li>{@code UserColumns.USER_EMAIL}</li>
     * </ul>
     * 
     * @param target      the String to be searched
     * @param columnName  column from TedTalksColumns interface
     * @return the String from columnName
     */
    public boolean search(String target, String columnName) {
        boolean found = false;
        String validUser = "";

        Cursor cur = mSqliteDb.rawQuery(SelectQuery.SEARCH_USER, new String[] {target});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    validUser = cur.getString(cur.getColumnIndex(columnName));
                    if (validUser.contentEquals(target)) {
                        found = true;
                        break;
                    }
                } while (cur.moveToNext());
            }
            cur.close();
        }
        return found;
    }

    /**
     * mNid and mType
     * 
     * @param nid    node_id
     * @param type   content_type (ted_talks, instructors, courses, etc)
     * @return found returns true if found otherwise false
     */
    public boolean isNodeIdFound(String nid, String type) {
        boolean found = false;
        String nodeId = null;
        String table  = null;
        String column = null;

        if (type.contentEquals("ted_talks")) {
            table  = Tables.TED_TALKS;
            column = TedTalksColumns.TED_TALKS_NID;

        } else if (type.contentEquals("instructor")) {
            table  = Tables.INSTRUCTORS;
            column = InstructorColumns.INSTRUCTOR_ID;

        } else if (type.contentEquals("ecorner_speakers")) {
            table  = Tables.ECORNER_SPEAKERS;
            column = ECornerSpeakerColumns.ECORNER_SPEAKER_ID;

        } else if (type.contentEquals("ecorner_video")) {
            table  = Tables.ECORNER_VIDEOS;
            column = ECornerVideoColumns.ECORNER_VIDEO_NID;

        } else if (type.contentEquals("course")) {
            table  = Tables.COURSES;
            column = CourseColumns.COURSE_ID;

        } else if (type.contentEquals("university")) {
            table  = Tables.UNIVERSITIES;
            column = UniversityColumns.UNIVERSITY_ID;

        } else if (type.contentEquals("class_groups")) {
            table  = Tables.CLASS_GROUPS;
            column = ClassGroupColumns.CLASS_GROUP_ID;

        } else if (type.contentEquals("ted_speakers")) {
            table  = Tables.TED_SPEAKERS;
            column = TedSpeakerColumns.TED_SPEAKER_ID;

        } else if (type.contentEquals("lesson")) {
            table  = Tables.LESSONS;
            column = LessonColumns.LESSON_ID;

        } else if (type.contentEquals("lesson_ted")) {
            table  = Tables.LESSONS_TED;
            column = LessonTedColumns.LESSON_TED_ID;

        } else if (type.contentEquals("lesson_ecorner")) {
            table  = Tables.LESSONS_ECORNER;
            column = LessonECornerColumns.LESSON_ECORNER_ID;

        } else if (type.contentEquals("lesson_videos")) {
            table  = Tables.LESSON_VIDEOS;
            column = LessonVideoColumns.LESSON_VIDEO_ID;
        }

        Cursor cur = mSqliteDb.rawQuery("SELECT * FROM " + table + " WHERE " + column + "=?", new String[] {nid});
        if (cur != null) {
            if (cur.moveToFirst()) {
                do {
                    nodeId = cur.getString(cur.getColumnIndex(column)); // TODO ecorner_video_id
                    if (nodeId.contentEquals(nid)) {
                        found = true;
                        break;
                    }
                } while (cur.moveToNext());
            }
            cur.close();
        }

        return found;
    }

    class DbHelper extends SQLiteOpenHelper {

        public DbHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
            mDbPath = Environment.getDataDirectory() + "/data/" + context.getPackageName() + "/databases/";
            boolean isDbExist = checkDatabase();
            if (isDbExist) {
                openDatabase();
            } else {
                createDatabase();
            }
        }

        private void createDatabase() {
            boolean isExist = checkDatabase();
            if (isExist) {
                // do nothing
            } else {
                copyDatabase();
            }
        }

        private void copyDatabase() {
            try {
                InputStream is = mContext.getAssets().open(DATABASE_NAME);
                String dbFile = mDbPath + DATABASE_NAME;
                File file = new File(dbFile);
                file.createNewFile();
                OutputStream os = new FileOutputStream(dbFile);

                byte[] buffer = new byte[1024];
                int length;
                while((length = is.read(buffer)) > 0) {
                    os.write(buffer, 0, length);
                }

                os.flush();
                os.close();
                is.close();
                DebugLog.d(TAG, "database >>>>>>> successfully copied");
                if (DebugLog.DEBUG_ON) {
                    Toast.makeText(mContext, "database successfully copied", Toast.LENGTH_SHORT).show();
                }

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        public void openDatabase() {
            String path = mDbPath + DATABASE_NAME;
            try {
                mSqliteDb = SQLiteDatabase.openDatabase(path, null, SQLiteDatabase.OPEN_READWRITE);
            } catch (SQLiteException e) {
                e.printStackTrace();
            }

        }

        public synchronized void closeDatabase() {
            if (mSqliteDb != null) mSqliteDb.close();
        }

        private boolean checkDatabase() {
            File f = new File(mDbPath);
            if (!f.exists()) {
                f.mkdir();
                DebugLog.d(TAG, "absolute_path=" + f.getAbsolutePath());
            }

            boolean isExist = false;
            try {
                File file = new File(mDbPath + DATABASE_NAME);
                DebugLog.d(TAG, "absolute_path=" + file.getAbsolutePath());
                if (file.isFile()) {
                    isExist = file.exists();
                } else if (file.isDirectory()) {
                    isExist = false;
                }

            } catch (SQLiteException e) {
                // doesnt exist
            }

            return isExist;
        }


        @Override
        public void onCreate(SQLiteDatabase db) {
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        }
    }
}
