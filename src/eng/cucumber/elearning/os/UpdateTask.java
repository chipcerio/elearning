package eng.cucumber.elearning.os;
/*
 * Copyright (C) 2013 Alistair Cerio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static eng.cucumber.elearning.util.Cheese.CONTENT_LAST_MODIFIED_URL;
import static eng.cucumber.elearning.util.Cheese.FORMAT_JSON;
import static eng.cucumber.elearning.util.Cheese.SVC_URL;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;
import eng.cucumber.elearning.app.MainActivity2;
import eng.cucumber.elearning.database.ELearningData;
import eng.cucumber.elearning.util.ELearningUtil;
import eng.cucumber.elearning.util.JSONManager;

public class UpdateTask extends AsyncTask<String, Void, Void> {
    private Context mContext;
    private ELearningData mDb;
    private String mDateRange;
    private String mType;
    private String mNid;
    
    public UpdateTask(Context context, ELearningData db, String dateRange) {
        mContext = context;
        mDb = db;
        mDateRange = dateRange;
    }
    
    private void fetchUpdates(HttpResponse response) {
        StringBuffer sb = new StringBuffer();
        HttpEntity entity = response.getEntity();
        try {
            InputStream is = entity.getContent();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            is.close();
            
            JSONObject obj = new JSONObject();
            JSONObject node = new JSONObject();
            
            JSONObject object = new JSONObject(sb.toString());
            JSONArray nodes = object.getJSONArray("nodes");
            for (int i = 0; i < nodes.length(); i++) {
                obj = nodes.getJSONObject(i);
                node = obj.getJSONObject("node");
                mType = node.getString("Type");
                fetchUpdateOnContent(mType);
            }
            
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Get updates by specific content_type
     * @param type content type
     */
    private void fetchUpdateOnContent(String type) {
        String url = CONTENT_LAST_MODIFIED_URL + mDateRange + "/" + type + FORMAT_JSON;
        HttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet(url);
        
        try {
            HttpResponse response = client.execute(get);
            StatusLine status = response.getStatusLine();
            int code = status.getStatusCode();
            
            switch (code) {
            case 200:
                fetchAndParseContent(response);
                break;
                
            case 400:
                break;
    
            default:
                break;
            }
            
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }

    private void fetchAndParseContent(HttpResponse response) {
        StringBuffer sb = new StringBuffer();
        HttpEntity entity = response.getEntity();
        try {
            InputStream is = entity.getContent();
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            is.close();
            
            JSONObject obj = new JSONObject();
            JSONObject node = new JSONObject();
            
            JSONObject object = new JSONObject(sb.toString());
            JSONArray nodes = object.getJSONArray("nodes");
            for (int i = 0; i < nodes.length(); i++) {
                obj = nodes.getJSONObject(i);
                node = obj.getJSONObject("node");
                mNid = node.getString("Nid");
                
                if (mDb.isNodeIdFound(mNid, mType)) {
                    httpGetRequest(mNid, JSONManager.UPDATE);
                } else {
                    // this may cause ANR
                    httpGetRequest(mNid, JSONManager.INSERT);
                }
            }
            
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    
    private void httpGetRequest(String nid, int operation) {
        StringBuilder sb = null;
        String line = null;
        HttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet(SVC_URL + nid);
        
        try {
            HttpResponse response = client.execute(get);
            StatusLine status = response.getStatusLine();
            int code = status.getStatusCode();
            
            switch (code) {
            case 200:
                HttpEntity entity = response.getEntity();
                InputStream is = entity.getContent();
                BufferedReader br = new BufferedReader(new InputStreamReader(is));
                
                sb = new StringBuilder();
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                }
                
                JSONManager.getInstance().setContext(mContext);
                JSONManager.getInstance().saveToDb(sb.toString(), operation);
                
                // after operation, update last-modified date
                String current = ELearningUtil.getCurrentDate();
                int id = mDb.getIdOnLastModifiedDate();
                JSONManager.getInstance().saveToDb(++id, current);
                
                break;
                
            case 400:
                break;

            default:
                break;
            }
            
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    @Override
    protected Void doInBackground(String... url) {
        HttpClient client = new DefaultHttpClient();
        HttpGet get = new HttpGet(url[0]);
        
        try {
            HttpResponse response = client.execute(get);
            StatusLine status = response.getStatusLine();
            int code = status.getStatusCode();
            
            switch (code) {
            case 200:
                fetchUpdates(response);
                break;
                
            case 400:
                 break;

            default:
                break;
            }
            
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        
        return null;
    }
    
    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        if (MainActivity2.sIsUpdating) {
            MainActivity2.setRefreshActionButtonState(false);
            Toast.makeText(mContext, "Update Finished", Toast.LENGTH_SHORT).show();
        }
    }

}
