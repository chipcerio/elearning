package eng.cucumber.elearning.test;
/*
 * Copyright (C) 2013 Alistair Cerio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static eng.cucumber.elearning.util.Cheese.SVC_URL;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;
import eng.cucumber.elearning.R;
import eng.cucumber.elearning.util.DebugLog;
import eng.cucumber.elearning.util.JSONManager;
import eng.cucumber.elearning.util.NetworkUtils;

public class HttpJsonParserActivity extends Activity {
    private TextView mText;
    private final static String TAG = "ParserActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.test);
        
        mText = (TextView) findViewById(R.id.text);
        mText.setText("No Data");
        
        new NetworkTask().execute();
    }
    
    /*
     * main functions send http requests, parse json, insert into db
     */
    class NetworkTask extends AsyncTask<Void, Void, String> {

        @Override
        protected String doInBackground(Void... params) {
            String type = "";
            
            
            if (NetworkUtils.isNetworkAvailable(HttpJsonParserActivity.this)) {
                
                HttpClient client = null;
                HttpGet get = null;
                HttpResponse response = null;
                StatusLine status = null;
                StringBuilder builder = null;
                for(int i = 0; i <= 280; i++) {
                    // i = 4;
                    /* http get */
                    client = new DefaultHttpClient();
                    get = new HttpGet(SVC_URL + Integer.toString(i));
                    
                    try {
                        response = client.execute(get);
                        status = response.getStatusLine();
                        
                        int code = status.getStatusCode();
                        if(code == 200 || code == 201) {
                            HttpEntity entity = response.getEntity();
                            InputStream is = entity.getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(is));
                            builder = new StringBuilder();
                            
                            String line;
                            while ((line = br.readLine()) != null) {
                                builder.append(line);
                            }
                            
                            JSONObject object = new JSONObject(builder.toString());
                            String title = object.getString("type");
                            DebugLog.d(TAG, "type=" + title);
                            object = null;
                            
                            // parse json and insert to db
                            JSONManager.getInstance().setContext(HttpJsonParserActivity.this);
                            JSONManager.getInstance().saveToDb(builder.toString(), JSONManager.INSERT);
                            
                        } else if(code == 403) {
                            DebugLog.d(TAG, "nid=" + i + " 403");
                        } else if(code == 404) {
                            DebugLog.d(TAG, "nid=" + i + " null");
                        } else if(code == 500) {
                            DebugLog.d(TAG, "nid=" + i + " 500");
                        }
                        
                    } catch (ClientProtocolException e) {
                        
                    } catch (IOException e) {
                        
                    } catch (JSONException e) {
                        
                    }
                }
                
            }
            
            
            return type; // type;
        }

        @Override
        protected void onPostExecute(String result) {
            // display
            result = "Done";
            mText.setText(result);
        }
        
    }

}
