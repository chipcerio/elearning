/*
 * Copyright (C) 2013 Alistair Cerio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eng.cucumber.elearning.util;

import android.util.Log;

public class DebugLog {
	public static final boolean DEBUG_ON = true; // toggle to false during release

	public static int d(String tag, String msg) {
		int response = -1;
		if (DEBUG_ON)
			response = Log.d(tag, msg);
		return response;
	}

	public static int e(String tag, String msg) {
		int response = -1;
		if (DEBUG_ON)
			response = Log.e(tag, msg);
		return response;
	}

	public static int i(String tag, String msg) {
		int response = -1;
		if (DEBUG_ON)
			response = Log.i(tag, msg);
		return response;
	}

	public static int v(String tag, String msg) {
		int response = -1;
		if (DEBUG_ON)
			response = Log.v(tag, msg);
		return response;
	}

	public static int w(String tag, String msg) {
		int response = -1;
		if (DEBUG_ON)
			response = Log.w(tag, msg);
		return response;
	}

}