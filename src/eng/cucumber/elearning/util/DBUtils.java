/*
 * Copyright (C) 2013 Alistair Cerio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eng.cucumber.elearning.util;

public class DBUtils {
    
    public interface InstructorColumns {
        String INSTRUCTOR_ID = "instructor_id";
        String INSTRUCTOR_TITLE = "instructor_title";
        String INSTRUCTOR_BODY = "instructor_body";
        String INSTRUCTOR_IMAGE = "instructor_photo";
        String INSTRUCTOR_COURSES = "instructor_courses";
        String INSTRUCTOR_NID = "instructor_nid";
        String INSTRUCTOR_TID = "instructor_tid";
    }
    
    public interface CourseColumns {
        String COURSE_ID = "course_id";
        String COURSE_TITLE = "course_title";
        String COURSE_BODY = "course_body";
        String COURSE_IMAGE = "course_image";
        String COURSE_PREREQUISITES = "course_prerequisites";
        String COURSE_TEXTBOOK = "course_textbook";
        String COURSE_FAQ = "course_faq";
        String COURSE_UNIVERSITY1 = "course_university1";
        String COURSE_UNIVERSITY2 = "course_university2";
        String COURSE_CATEGORY1 = "course_category1";
        String COURSE_CATEGORY2 = "course_category2";
        String COURSE_INSTRUCTOR1 = "course_instructor1";
        String COURSE_INSTRUCTOR2 = "course_instructor2";
        String COURSE_DURATION = "course_duration";
        String COURSE_NEXTSESSION = "course_nextsession";
        String COURSE_GROUP_AUDIENCE = "course_group_audience";
        String COURSE_NID = "course_nid";
        String COURSE_TID = "course_tid";
    }
    
    public interface CourseCategoryColumns {
        String COURSE_CATEGORY_ID = "course_category_id";
        String CATEGORY_ID = "category_id";
        String COURSE_ID = "course_id";
    }
    
    public interface UniversityColumns {
        String UNIVERSITY_ID = "university_id";
        String UNIVERSITY_TITLE = "university_title";
        String UNIVERSITY_BODY = "university_body";
        String UNIVERSITY_BANNER = "university_banner";
        String UNIVERSITY_NID = "university_nid";
        String UNIVERSITY_TID = "university_tid";
    }
    
    public interface LessonColumns {
        String LESSON_ID = "lesson_id";
        String LESSON_TITLE = "lesson_title";
        String LESSON_BODY = "lesson_body";
        String LESSON_NID = "lesson_nid";
        String LESSON_TID = "lesson_tid";
    }
    
    public interface ClassGroupColumns {
        String CLASS_GROUP_ID = "class_group_id";
        String CLASS_GROUP_TITLE = "class_group_title";
        String CLASS_GROUP_BODY = "class_group_body";
        String CLASS_GROUP_NID = "class_group_nid";
        String CLASS_GROUP_TID = "class_group_tid";
    }
    
    public interface LessonVideoColumns {
        String LESSON_VIDEO_ID = "lesson_videos_id";
        String LESSON_VIDEO_TITLE = "lesson_videos_title";
        String LESSON_VIDEO_BODY = "lesson_videos_body";
        String LESSON_VIDEO_URL = "lesson_videos_url";
        String LESSON_VIDEO_SRT = "lesson_videos_srt";
        String LESSON_VIDEO_NID = "lesson_video_nid";
        String LESSON_VIDEO_TID = "lesson_video_tid";
    }
    
    public interface TedSpeakerColumns {
        String TED_SPEAKER_ID = "ted_speaker_id";
        String TED_SPEAKER_NAME = "ted_speaker_name";
        String TED_SPEAKER_BODY = "ted_speaker_body";
        String TED_SPEAKER_TITLE = "ted_speaker_title";
        String TED_SPEAKER_PHOTO = "ted_speaker_photo";
        String TED_SPEAKER_BIO = "ted_speaker_bio";
        String TED_SPEAKER_NID = "ted_speaker_nid";
        String TED_SPEAKER_TID = "ted_speaker_tid";
    }
    
    public interface TedTalksColumns {
        String TED_TALKS_ID = "ted_talks_id";
        String TED_TALKS_TITLE = "ted_talks_title";
        String TED_TALKS_BODY = "ted_talks_body";
        String TED_TALKS_INSTRUCTOR1 = "ted_talks_instructor1";
        String TED_TALKS_INSTRUCTOR2 = "ted_talks_instructor2";
        String TED_TALKS_IMAGE = "ted_talks_image";
        String TED_TALKS_VIDEO = "ted_talks_video";
        String TED_TALKS_MP3 = "ted_talks_mp3";
        String TED_TALKS_MP3_LOCAL = "ted_talks_mp3_local";
        String TED_TALKS_SRT = "ted_talks_srt";
        String TED_TALKS_SRT_LOCAL = "ted_talks_srt_local";
        String TED_TALKS_SRT_CN = "ted_talks_cn";
        String TED_TALKS_SRT_CN_LOCAL = "ted_talks_cn_local";
        String TED_TALKS_VIDEO_LD = "ted_talks_video_ld";
        String TED_TALKS_VIDEO_LD_LOCAL = "ted_talks_video_ld_local";
        String TED_TALKS_VIDEO_SD = "ted_talks_video_sd";
        String TED_TALKS_VIDEO_SD_LOCAL = "ted_talks_video_sd_local";
        String TED_TALKS_VIDEO_HD = "ted_talks_video_hd";
        String TED_TALKS_VIDEO_HD_LOCAL = "ted_talks_video_hd_local";
        String TED_TALKS_CATEGORY1 = "ted_talks_category1";
        String TED_TALKS_CATEGORY2 = "ted_talks_category2";
        String TED_TALKS_NID = "ted_talks_nid";
        String TED_TALKS_TID = "ted_talks_tid";
        String TED_TALKS_GROUP = "ted_talks_group";
    }
    
    public interface ECornerSpeakerColumns {
        String ECORNER_SPEAKER_ID = "ecorner_speakers_id";
        String ECORNER_SPEAKER_TITLE = "ecorner_speakers_title";
        String ECORNER_SPEAKER_BODY = "ecorner_speakers_body";
        String ECORNER_SPEAKER_IMAGE = "ecorner_speakers_image";
        String ECORNER_SPEAKER_COMPANY = "ecorner_speakers_company";
        String ECORNER_SPEAKER_WWW = "ecorner_speakers_www";
        String ECORNER_SPEAKER_NID = "ecorner_speakers_nid";
        String ECORNER_SPEAKER_TID = "ecorner_speakers_tid";
    }
    
    public interface ECornerVideoColumns {
        String ECORNER_VIDEO_ID = "ecorner_video_id";
        String ECORNER_VIDEO_TITLE = "ecorner_video_title";
        String ECORNER_VIDEO_BODY = "ecorner_video_body";
        String ECORNER_VIDEO_INSTRUCTOR1 = "ecorner_video_instructor1";
        String ECORNER_VIDEO_INSTRUCTOR2 = "ecorner_video_instructor2";
        String ECORNER_VIDEO_IMAGE = "ecorner_video_image";
        String ECORNER_VIDEO_CATEGORY1 = "ecorner_video_category1";
        String ECORNER_VIDEO_CATEGORY2 = "ecorner_video_category2";
        String ECORNER_VIDEO_VIDEO = "ecorner_video_video";
        String ECORNER_VIDEO_LOCAL = "ecorner_video_local";
        String ECORNER_VIDEO_SRT = "ecorner_video_srt";
        String ECORNER_VIDEO_SRT_LOCAL = "ecorner_video_srt_local";
        String ECORNER_VIDEO_SRT_CN = "ecorner_video_cn";
        String ECORNER_VIDEO_SRT_CN_LOCAL = "ecorner_video_cn_local";
        String ECORNER_VIDEO_MP3 = "ecorner_video_mp3";
        String ECORNER_VIDEO_MP3_LOCAL = "ecorner_video_mp3_local";
        String ECORNER_VIDEO_NID = "ecorner_video_nid";
        String ECORNER_VIDEO_TID = "ecorner_video_tid";
        String ECORNER_VIDEO_GROUP = "ecorner_video_group";
    }
    
    public interface LessonTedColumns {
        String LESSON_TED_ID = "lesson_ted_id";
        String LESSON_TED_TITLE = "lesson_ted_title";
        String LESSON_TED_BODY = "lesson_ted_body";
        String LESSON_TED_CATEGORY = "lesson_ted_category";
        String LESSON_TED_NID = "lesson_ted_nid";
        String LESSON_TED_TID = "lesson_ted_tid";
    }
    
    public interface LessonECornerColumns {
        String LESSON_ECORNER_ID = "lesson_ecorner_id";
        String LESSON_ECORNER_TITLE = "lesson_ecorner_title";
        String LESSON_ECORNER_BODY = "lesson_ecorner_body";
        String LESSON_ECORNER_CATEGORY = "lesson_ecorner_category";
        String LESSON_ECORNER_NID = "lesson_ecorner_nid";
        String LESSON_ECORNER_TID = "lesson_ecorner_tid";
    }
    
    public interface AssignmentColumns {
        String ASSIGNMENT_ID = "assignment_id";
        String ASSIGNMENT_TITLE = "assignment_title";
        String ASSIGNMENT_BODY = "assignment_body";
        String ASSIGNMENT_NID = "assignment_nid";
        String ASSIGNMENT_TID = "assignment_tid";
    }
    
    public interface UserColumns {
        String USER_ID = "user_id";
        String USER_NAME = "user_name";
        String USER_PASSWORD_HASH = "user_password_hash";
        String USER_SALT = "user_salt";
        String USER_EMAIL = "user_email";
    }

    public interface CategoryColumns {
        String CATEGORY_ID = "category_id";
        String CATEGORY_TITLE = "category_title";
    }
    
    public interface UpdatesColumns {
        String UPDATE_ID = "update_id";
        String UPDATE_LAST_MODIFIED = "update_last_modified";
    }
}
