package eng.cucumber.elearning.util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import eng.cucumber.elearning.database.ELearningDb;

import android.content.Context;

public class JSONManager {
    private static JSONManager sInstance;
    private Context mContext;
    private int mOperation;
    
    public static final int INSERT = 1;
    public static final int UPDATE = 2;

    public static JSONManager getInstance() {
        if (sInstance == null) {
            sInstance = new JSONManager();
            return sInstance;
        } else {
            return sInstance;
        }
    }
    
    /**
     * Saves the data to the content type specified
     * @param jsonString content type
     * @param operation  JSONManager insert/update operation
     */
    public void saveToDb(String jsonString, int operation) {
        mOperation = operation;
        
        try {
            JSONObject json = new JSONObject(jsonString);
            String type = json.getString("type");
            
            if (type.equalsIgnoreCase("instructor")) {
                saveToInstructorTable(jsonString);
            } else if (type.equalsIgnoreCase("course")) {
                saveToCourseTable(jsonString);
            } else if (type.equalsIgnoreCase("university")) {
                saveToUniversityTable(jsonString);
            } else if (type.equalsIgnoreCase("lesson")) {
                saveToLessonTable(jsonString);
            } else if (type.equalsIgnoreCase("lesson_ted")) {
                saveToLessonTedTable(jsonString);
            } else if (type.equalsIgnoreCase("lesson_ecorner")) {
                saveToLessonECornerTable(jsonString);
            } else if (type.equalsIgnoreCase("lesson_videos")) {
                saveToLessonVideoTable(jsonString);
            } else if (type.equalsIgnoreCase("class_groups")) {
                saveToClassGroupsTable(jsonString);
            } else if (type.equalsIgnoreCase("ted_speakers")) {
                saveToTedSpeakerTable(jsonString);
            } else if (type.equalsIgnoreCase("ted_talks")) {
                saveToTedTalksTable(jsonString);
            } else if (type.equalsIgnoreCase("ecorner_speakers")) {
                saveToECornerSpeakersTable(jsonString);
            } else if (type.equalsIgnoreCase("ecorner_video")) {
                saveToECornerVideoTable(jsonString);
            } 
            
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    
    public void saveToDb(int id, String curDate) {
        ELearningDb db = new ELearningDb(mContext);
        db.open();
        db.insertUpdatesRow(id, curDate);
        db.close();
    }
    
    private void saveToInstructorTable(String jsonString) {
        int nid = 0, tid = 0;
        String title = "", body = "", filename = "", courses = "";
        
        try {
            JSONObject object = new JSONObject(jsonString);
            
            // nid
            nid = object.getInt("nid");
            
            // tid
    
            // title
            title = object.getString("title");
            
            // body
            JSONObject jsonBody = object.getJSONObject("body");
            JSONArray arrBody = jsonBody.getJSONArray("und");
            for(int i = 0; i < arrBody.length(); i++) {
                JSONObject obj = arrBody.getJSONObject(i);
                if(obj.has("value")) {
                    body = obj.getString("value");
                }
            }
            
            // photo
            JSONObject photo = object.getJSONObject("field_instructor_photo");
            JSONArray undphoto = photo.getJSONArray("und");
            for (int i = 0; i < undphoto.length(); i++) {
                JSONObject obj = undphoto.getJSONObject(i);
                if(obj.has("filename")) {
                    filename = obj.getString("filename");
                }
            }
            
            ELearningDb db = new ELearningDb(mContext);
            db.open();
            switch (mOperation) {
            case INSERT:
                db.insertInstructorRow(nid, tid, title, body, filename, courses);
                break;
                
            case UPDATE:
                db.updateInstructorRow(nid, tid, title, body, filename, courses);
                break;

            default:
                break;
            }
            db.close();
            
        } catch(JSONException e) {
            e.printStackTrace();
        }
    }

    private void saveToCourseTable(String jsonString) {
        int nid = 0, tid = 0, 
            university1 = 0, university2 = 0,
            instructor1 = 0, instructor2 = 0,
            category1 = 0, category2 = 0,
            groupAudience = 0;
        
        String title = "", 
               body = "", 
               image = "", 
               prerequisite = "", 
               textbook = "", 
               faq = "",
               duration = "",
               nextSession = "";
        
        try {
            JSONObject object = new JSONObject(jsonString);
            
            // title
            title = object.getString("title");
            
            // nid
            nid = object.getInt("nid");
            
            // tid
            
            // body
            JSONObject jsonBody = object.getJSONObject("body");
            JSONArray arrBody = jsonBody.getJSONArray("und");
            for(int i = 0; i < arrBody.length(); i++) {
                JSONObject obj = arrBody.getJSONObject(i);
                if (obj.has("value")) {
                    body = obj.getString("value");
                }
            }
            
            // image
            JSONObject jsonImage = object.getJSONObject("field_course_image");
            JSONArray arrImage = jsonImage.getJSONArray("und");
            for (int i = 0; i < arrImage.length(); i++) {
                JSONObject obj = arrImage.getJSONObject(i);
                if (obj.has("filename")) {
                    image = obj.getString("filename");
                }
            }
            
            // prerequisites
            JSONObject jsonPrerequisite = object.getJSONObject("field_course_prerequisites");
            JSONArray arrPrerequisite = jsonPrerequisite.getJSONArray("und");
            for (int i = 0; i < arrPrerequisite.length(); i++) {
                JSONObject obj = arrPrerequisite.getJSONObject(i);
                if (obj.has("value")) {
                    prerequisite = obj.getString("value");
                }
            } 
            
            // textbook
            JSONObject jsonTextbook = object.getJSONObject("field_course_textbook");
            JSONArray arrTextbook = jsonTextbook.getJSONArray("und");
            for (int i = 0; i < arrTextbook.length(); i++) {
                JSONObject obj = arrTextbook.getJSONObject(i);
                if (obj.has("value")) {
                    textbook = obj.getString("value");
                }
            }
            
            // faq
            JSONObject jsonFaq = object.getJSONObject("field_course_faq");
            JSONArray arrFaq = jsonFaq.getJSONArray("und");
            for (int i = 0; i < arrFaq.length(); i++) {
                JSONObject obj = arrFaq.getJSONObject(i);
                if (obj.has("value")) {
                    faq = obj.getString("value");
                }
            }
            
            // university
            JSONObject jsonUniversity = object.getJSONObject("field_course_university");
            JSONArray arrUniversity = jsonUniversity.getJSONArray("und");
            JSONObject targetUniversity = null;
            for (int i = 0; i < arrUniversity.length(); i++) {
                targetUniversity = arrUniversity.getJSONObject(i);
                if (targetUniversity.has("tid")) {
                    switch (i) {
                    case 0:
                        university1 = Integer.parseInt(targetUniversity.getString("tid"));
                        break;
                        
                    case 1:
                        university2 = Integer.parseInt(targetUniversity.getString("tid"));
                        break;
    
                    default:
                        break;
                    }
                }
                targetUniversity = null;
            }
            
            // category
            JSONObject jsonCategory = object.getJSONObject("field_course_category");
            JSONArray arrCategory = jsonCategory.getJSONArray("und");
            JSONObject targetCategory = null;
            for (int i = 0; i < arrCategory.length(); i++) {
                targetCategory = arrCategory.getJSONObject(i);
                if (targetCategory.has("tid")) {
                    switch (i) {
                    case 0:
                        category1 = Integer.parseInt(targetCategory.getString("tid"));
                        break;
                        
                    case 1:
                        category2 = Integer.parseInt(targetCategory.getString("tid"));
                        break;
    
                    default:
                        break;
                    }
                }
                targetUniversity = null;
            }
            
            // instructor
            JSONObject jsonInstructor = object.getJSONObject("field_course_instructor");
            JSONArray arrInstructor = jsonInstructor.getJSONArray("und");
            JSONObject targetInstructor = null;
            for (int i = 0; i < arrInstructor.length(); i++) {
                targetInstructor = arrInstructor.getJSONObject(i);
                if (targetInstructor.has("nid")) {
                    switch (i) {
                    case 0:
                        instructor1 = Integer.parseInt(targetInstructor.getString("nid"));
                        break;
                        
                    case 1:
                        instructor2 = Integer.parseInt(targetInstructor.getString("nid"));
                        break;
    
                    default:
                        break;
                    }
                }
                targetInstructor = null;
            }
            
            // duration
            JSONObject jsonDuration = object.getJSONObject("field_course_duration");
            JSONArray arrDuration = jsonDuration.getJSONArray("und");
            for (int i = 0; i < arrDuration.length(); i++) {
                JSONObject obj = arrDuration.getJSONObject(i);
                if (obj.has("value")) {
                    duration = obj.getString("value");
                }
            }
            
            // next session
            JSONObject jsonNextSession = object.getJSONObject("field_course_nextsession");
            JSONArray arrNextSession = jsonNextSession.getJSONArray("und");
            for (int i = 0; i < arrNextSession.length(); i++) {
                JSONObject obj = arrNextSession.getJSONObject(i);
                if (obj.has("value")) {
                    nextSession = obj.getString("value");
                }
            }
            
            // group audience returns an object or array
            Object o = object.get("group_audience");
            if (o instanceof JSONObject) {
                JSONObject jsonGroupAudience = object.getJSONObject("group_audience");
                JSONArray arrGroupAudience = jsonGroupAudience.getJSONArray("und");
                for (int i = 0; i < arrGroupAudience.length(); i++) {
                    JSONObject obj = arrGroupAudience.getJSONObject(i);
                    if (obj.has("value")) {
                        groupAudience = Integer.parseInt(obj.getString("gid"));
                    }
                }
            
            } else if (o instanceof JSONArray) {
                // do nothing
            }
            
            
            ELearningDb db = new ELearningDb(mContext);
            db.open();
            switch (mOperation) {
            case INSERT:
                db.insertCourseRow(title, nid, tid, body, image, prerequisite, textbook, faq, 
                        university1, university2, category1, category2, instructor1, instructor2,
                        duration, nextSession, groupAudience);
                break;
                
            case UPDATE:
                db.updateCourseRow(title, nid, tid, body, image, prerequisite, textbook, faq, 
                        university1, university2, category1, category2, instructor1, instructor2,
                        duration, nextSession, groupAudience);
                break;

            default:
                break;
            }
            db.close();
            
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void saveToUniversityTable(String jsonString) {
        int nid = 0, tid = 0;
        String title = "", body = "", banner = "";
               
        try {
            JSONObject object = new JSONObject(jsonString);

            // nid
            nid = object.getInt("nid");
            
            // tid
            
            // title
            title = object.getString("title");
            
            // body
            Object o = object.get("body");
            if (o instanceof JSONObject) {
                JSONObject jsonBody = object.getJSONObject("body");
                JSONArray arrBody = jsonBody.getJSONArray("und");
                for(int i = 0; i < arrBody.length(); i++) {
                    JSONObject obj = arrBody.getJSONObject(i);
                    if (obj.has("value")) {
                        body = obj.getString("value");
                    }
                }
                
            } else if (o instanceof JSONArray) {
                // do nothing
            }
            
            // banner
            JSONObject jsonBanner = object.getJSONObject("field_banner");
            JSONArray arrBanner = jsonBanner.getJSONArray("und");
            for(int i = 0; i < arrBanner.length(); i++) {
                JSONObject obj = arrBanner.getJSONObject(i);
                if (obj.has("filename")) {
                    banner = obj.getString("filename");
                }
            }
            
            ELearningDb db = new ELearningDb(mContext);
            db.open();
            switch (mOperation) {
            case INSERT:
                db.insertUniversityRow(nid, tid, title, body, banner);
                break;
                
            case UPDATE:
                db.updateUniversityRow(nid, tid, title, body, banner);
                break;

            default:
                break;
            }
            db.close();
            
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    
    private void saveToLessonTable(String jsonString) {
        int nid = 0, tid = 0;
        String title = "", body = "";
        
        try {
            JSONObject object = new JSONObject(jsonString);
            
            // nid
            nid = object.getInt("nid");
            
            // tid
            
            // title
            title = object.getString("title");
            
            // body
            JSONObject jsonBody = object.getJSONObject("body");
            JSONArray arrBody = jsonBody.getJSONArray("und");
            for(int i = 0; i < arrBody.length(); i++) {
                JSONObject obj = arrBody.getJSONObject(i);
                if (obj.has("value")) {
                    body = obj.getString("value");
                }
            }
            
            ELearningDb db = new ELearningDb(mContext);
            db.open();
            switch (mOperation) {
            case INSERT:
                db.insertLessonRow(nid, tid, title, body);
                break;
                
            case UPDATE:
                db.updateLessonRow(nid, tid, title, body);
                break;

            default:
                break;
            }
            db.close();
            
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
        
    }

    private void saveToLessonTedTable(String jsonString) {
        int nid = 0, tid = 0, category = 0;
        String title = "", body = "";
        
        try {
            JSONObject object = new JSONObject(jsonString);
            
            // nid
            nid = object.getInt("nid");
            
            // tid
            
            // title
            title = object.getString("title");
            
            // body
            JSONObject jsonBody = object.getJSONObject("body");
            JSONArray arrBody = jsonBody.getJSONArray("und");
            for(int i = 0; i < arrBody.length(); i++) {
                JSONObject obj = arrBody.getJSONObject(i);
                if (obj.has("value")) {
                    body = obj.getString("value");
                }
            }
            // category
            JSONObject jsonCategory = object.getJSONObject("field_lesson_ted_category");
            JSONArray arrCategory = jsonCategory.getJSONArray("und");
            for(int i = 0; i < arrCategory.length(); i++) {
                JSONObject obj = arrCategory.getJSONObject(i);
                if (obj.has("tid")) {
                    category = obj.getInt("tid");
                }
            }
            
            // db
            ELearningDb db = new ELearningDb(mContext);
            db.open();
            switch (mOperation) {
            case INSERT:
                db.insertLessonTedRow(nid, tid, title, body, category);
                break;
                
            case UPDATE:
                db.updateLessonTedRow(nid, tid, title, body, category);
                break;

            default:
                break;
            }
            db.close();
            
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void saveToLessonECornerTable(String jsonString) {
        int nid = 0, tid = 0, category = 0;
        String title = "", body = "";
        
        try {
            JSONObject object = new JSONObject(jsonString);
            
            // nid
            nid = object.getInt("nid");
            
            // tid
            
            // title
            title = object.getString("title");
            
            // body
            
            // category
            JSONObject jsonCategory = object.getJSONObject("field_lesson_ecorner_category");
            JSONArray arrCategory = jsonCategory.getJSONArray("und");
            for(int i = 0; i < arrCategory.length(); i++) {
                JSONObject obj = arrCategory.getJSONObject(i);
                if (obj.has("tid")) {
                    category = obj.getInt("tid");
                }
            }
            
            // db
            ELearningDb db = new ELearningDb(mContext);
            db.open();
            switch (mOperation) {
            case INSERT:
                db.insertLessonECornerRow(nid, tid, title, body, category);
                break;
                
            case UPDATE:
                db.updateLessonECornerRow(nid, tid, title, body, category);
                break;

            default:
                break;
            }
            db.close();
            
        } catch (JSONException e) {
            throw new RuntimeException(e);
        }
    }

    private void saveToLessonVideoTable(String jsonString) {
        // nid, tid, title, body, videoUrl, videoSrt
        int nid = 0, tid = 0;
        String title = "", body = "", videoUrl = "", videoSrt = "";
        
        try {
            JSONObject object = new JSONObject(jsonString);
            
            // nid
            nid = object.getInt("nid");
            
            // tid
            
            // title
            title = object.getString("title");
            
            // body
            JSONObject jsonBody = object.getJSONObject("body");
            JSONArray arrBody = jsonBody.getJSONArray("und");
            for(int i = 0; i < arrBody.length(); i++) {
                JSONObject obj = arrBody.getJSONObject(i);
                if(obj.has("value")) {
                    body = obj.getString("value");
                }
            }
            
            // videoUrl
            JSONObject jsonVideoUrl = object.getJSONObject("field_video_url");
            JSONArray arrVideoUrl = jsonVideoUrl.getJSONArray("und");
            for(int i = 0; i < arrVideoUrl.length(); i++) {
                JSONObject obj = arrVideoUrl.getJSONObject(i);
                if(obj.has("value")) {
                    videoUrl = obj.getString("value");
                }
            }
            
            // videoSrt
            JSONObject jsonVideoSrt = object.getJSONObject("field_video_srt");
            JSONArray arrVideoSrt = jsonVideoSrt.getJSONArray("und");
            for(int i = 0; i < arrVideoSrt.length(); i++) {
                JSONObject obj = arrVideoSrt.getJSONObject(i);
                if(obj.has("value")) {
                    videoSrt = obj.getString("value");
                }
            }
            
            ELearningDb db = new ELearningDb(mContext);
            db.open();
            switch (mOperation) {
            case INSERT:
                db.insertLessonVideoRow(nid, tid, title, body, videoUrl, videoSrt);
                break;
                
            case UPDATE:
                db.updateLessonVideoRow(nid, tid, title, body, videoUrl, videoSrt);
                break;

            default:
                break;
            }
            db.close();
            
            
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
    }

    private void saveToClassGroupsTable(String jsonString) {
        // nid, tid, title, body
        int nid = 0, tid = 0;
        String title = "", body = "";
        
        try {
            JSONObject object = new JSONObject(jsonString);
            
         // nid
            nid = object.getInt("nid");
            
            // tid
            
            // title
            title = object.getString("title");
            
            // body
            JSONObject jsonBody = object.getJSONObject("body");
            JSONArray arrBody = jsonBody.getJSONArray("und");
            for(int i = 0; i < arrBody.length(); i++) {
                JSONObject obj = arrBody.getJSONObject(i);
                if(obj.has("value")) {
                    body = obj.getString("value");
                }
            }
            
            ELearningDb db = new ELearningDb(mContext);
            db.open();
            switch (mOperation) {
            case INSERT:
                db.insertClassGroupsRow(nid, tid, title, body);
                break;
                
            case UPDATE:
                db.updateClassGroupsRow(nid, tid, title, body);
                break;

            default:
                break;
            }
            db.close();
            
        } catch (JSONException e) {
            e.printStackTrace();
        }
        
    }

    private void saveToTedSpeakerTable(String jsonString) {
        // nid, tid, title, body, speaker title, photo, bio
        int nid = 0, tid = 0;
        String title = "", body = "", 
               speakerTitle = "", photo = "", bio = "";
        
        try {
            JSONObject object = new JSONObject(jsonString);
            
            // nid
            nid = object.getInt("nid");
            
            // tid
    
            // title
            title = object.getString("title");
            
            // body
            JSONObject jsonBody = object.getJSONObject("body");
            JSONArray arrBody = jsonBody.getJSONArray("und");
            for(int i = 0; i < arrBody.length(); i++) {
                JSONObject obj = arrBody.getJSONObject(i);
                if(obj.has("value")) {
                    body = obj.getString("value");
                }
            }
            
            // speaker
            JSONObject jsonSpeakerTitle = object.getJSONObject("field_ted_speakers_title");
            JSONArray arrSpeakerTitle = jsonSpeakerTitle.getJSONArray("und");
            for(int i = 0; i < arrSpeakerTitle.length(); i++) {
                JSONObject obj = arrSpeakerTitle.getJSONObject(i);
                if(obj.has("value")) {
                    speakerTitle = obj.getString("value");
                }
            }
            
            // photo
            JSONObject jsonPhoto = object.getJSONObject("field_ted_speakers_photo");
            JSONArray arrPhoto = jsonPhoto.getJSONArray("und");
            for(int i = 0; i < arrPhoto.length(); i++) {
                JSONObject obj = arrPhoto.getJSONObject(i);
                if(obj.has("filename")) {
                    photo = obj.getString("filename");
                }
            }
            
            // bio
            JSONObject jsonBio = object.getJSONObject("field_ted_speakers_bio");
            JSONArray arrBio = jsonBio.getJSONArray("und");
            for(int i = 0; i < arrBio.length(); i++) {
                JSONObject obj = arrBio.getJSONObject(i);
                if(obj.has("value")) {
                    bio = obj.getString("value");
                }
            }
            
            ELearningDb db = new ELearningDb(mContext);
            db.open();
            
            switch (mOperation) {
            case INSERT:
                db.insertTedSpeakerRow(nid, tid, title, body, speakerTitle, photo, bio);
                break;
                
            case UPDATE:
                db.updateTedSpeakerRow(nid, tid, title, body, speakerTitle, photo, bio);
                break;

            default:
                break;
            }
            db.close();
            
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void saveToTedTalksTable(String jsonString) {
        int nid = 0, tid = 0, author1 = 0, author2 = 0, category1 = 0, category2 = 0;
        String title = "", body = "",
               image = "", video = "",
               mp3 = "", mp3Local = "", 
               srt = "", srtLocal = "", 
               srtCn = "", srtCnLocal = "",
               videoLd = "", videoLdLocal = "",
               videoSd = "", videoSdLocal = "",
               videoHd = "", videoHdLocal = "";
        
        try {
            JSONObject object = new JSONObject(jsonString);
            
            // nid
            nid = object.getInt("nid");
            
            // tid
    
            // title
            title = object.getString("title");
            
            // body
            JSONObject jsonBody = object.getJSONObject("body");
            JSONArray arrBody = jsonBody.getJSONArray("und");
            for(int i = 0; i < arrBody.length(); i++) {
                JSONObject obj = arrBody.getJSONObject(i);
                if(obj.has("value")) {
                    body = obj.getString("value");
                }
            }
            
            // image
            JSONObject jsonImage = object.getJSONObject("field_ted_talk_image");
            JSONArray arrImage = jsonImage.getJSONArray("und");
            for(int i = 0; i < arrImage.length(); i++) {
                JSONObject obj = arrImage.getJSONObject(i);
                if(obj.has("filename")) {
                    image = obj.getString("filename");
                }
            }
            
            // video
            JSONObject jsonVideo = object.getJSONObject("field_ted_talk_video");
            JSONArray arrVideo = jsonVideo.getJSONArray("und");
            for(int i = 0; i < arrVideo.length(); i++) {
                JSONObject obj = arrVideo.getJSONObject(i);
                if(obj.has("value")) {
                    video = obj.getString("value");
                }
            }
            
            // author
            JSONObject jsonAuthor = object.getJSONObject("field_ted_talk_author");
            JSONArray arrAuthor = jsonAuthor.getJSONArray("und");
            JSONObject targetAuthor = null;
            for(int i = 0; i < arrAuthor.length(); i++) {
                targetAuthor = arrAuthor.getJSONObject(i);
                if(targetAuthor.has("nid")) {
                    switch (i) {
                    case 0:
                        author1 = Integer.parseInt(targetAuthor.getString("nid"));
                        break;
                        
                    case 1:
                        author2 = Integer.parseInt(targetAuthor.getString("nid"));
                        break;
    
                    default:
                        break;
                    }
                }
                targetAuthor = null;
            }
            
            // srt
            JSONObject jsonSrt = object.getJSONObject("field_ted_talk_srt");
            JSONArray arrSrt = jsonSrt.getJSONArray("und");
            for(int i = 0; i < arrSrt.length(); i++) {
                JSONObject obj = arrSrt.getJSONObject(i);
                if(obj.has("value")) {
                    srt = obj.getString("value");
                }
            }
            
            // srt_cn
            JSONObject jsonSrtCn = object.getJSONObject("field_ted_talk_srt_cn");
            JSONArray arrSrtCn = jsonSrtCn.getJSONArray("und");
            for(int i = 0; i < arrSrtCn.length(); i++) {
                JSONObject obj = arrSrtCn.getJSONObject(i);
                if(obj.has("value")) {
                    srtCn = obj.getString("value");
                }
            }
            
            // video_ld
            JSONObject jsonVideoLd = object.getJSONObject("field_ted_talk_video_ld");
            JSONArray arrVideoLd = jsonVideoLd.getJSONArray("und");
            for(int i = 0; i < arrVideoLd.length(); i++) {
                JSONObject obj = arrVideoLd.getJSONObject(i);
                if(obj.has("value")) {
                    videoLd = obj.getString("value");
                }
            }
            
            // video_ld_local
            JSONObject jsonVideoLdLocal = object.getJSONObject("field_ted_talk_video_ld_local");
            JSONArray arrVideoLdLocal = jsonVideoLdLocal.getJSONArray("und");
            for(int i = 0; i < arrVideoLdLocal.length(); i++) {
                JSONObject obj = arrVideoLdLocal.getJSONObject(i);
                if(obj.has("value")) {
                    videoLdLocal = obj.getString("value");
                }
            }
            
            // video_sd
            JSONObject jsonVideoSd = object.getJSONObject("field_ted_talk_video_sd");
            JSONArray arrVideoSd = jsonVideoSd.getJSONArray("und");
            for(int i = 0; i < arrVideoSd.length(); i++) {
                JSONObject obj = arrVideoSd.getJSONObject(i);
                if(obj.has("value")) {
                    videoSd = obj.getString("value");
                }
            }
            
            // video_sd_local
            JSONObject jsonVideoSdLocal = object.getJSONObject("field_ted_talk_video_sd_local");
            JSONArray arrVideoSdLocal = jsonVideoSdLocal.getJSONArray("und");
            for(int i = 0; i < arrVideoSdLocal.length(); i++) {
                JSONObject obj = arrVideoSdLocal.getJSONObject(i);
                if(obj.has("value")) {
                    videoSdLocal = obj.getString("value");
                }
            }
            
            // video_hd
            JSONObject jsonVideoHd = object.getJSONObject("field_ted_talk_video_hd");
            JSONArray arrVideoHd = jsonVideoHd.getJSONArray("und");
            for(int i = 0; i < arrVideoHd.length(); i++) {
                JSONObject obj = arrVideoHd.getJSONObject(i);
                if(obj.has("value")) {
                    videoHd = obj.getString("value");
                }
            }
            
            // video_sd_local
            JSONObject jsonVideoHdLocal = object.getJSONObject("field_ted_talk_video_hd_local");
            JSONArray arrVideoHdLocal = jsonVideoHdLocal.getJSONArray("und");
            for(int i = 0; i < arrVideoHdLocal.length(); i++) {
                JSONObject obj = arrVideoHdLocal.getJSONObject(i);
                if(obj.has("value")) {
                    videoHdLocal = obj.getString("value");
                }
            }
            
            // mp3
            JSONObject jsonMp3 = object.getJSONObject("field_ted_talk_mp3");
            JSONArray arrMp3 = jsonMp3.getJSONArray("und");
            for(int i = 0; i < arrMp3.length(); i++) {
                JSONObject obj = arrMp3.getJSONObject(i);
                if(obj.has("value")) {
                    mp3 = obj.getString("value");
                }
            }
            
            // mp3_local
            JSONObject jsonMp3Local = object.getJSONObject("field_ted_talk_mp3_local");
            JSONArray arrMp3Local = jsonMp3Local.getJSONArray("und");
            for(int i = 0; i < arrMp3Local.length(); i++) {
                JSONObject obj = arrMp3Local.getJSONObject(i);
                if(obj.has("value")) {
                    mp3Local = obj.getString("value");
                }
            }
            
            // srt_local
            JSONObject jsonSrtLocal = object.getJSONObject("field_ted_talk_srt_local");
            JSONArray arrSrtLocal = jsonSrtLocal.getJSONArray("und");
            for(int i = 0; i < arrSrtLocal.length(); i++) {
                JSONObject obj = arrSrtLocal.getJSONObject(i);
                if(obj.has("value")) {
                    srtLocal = obj.getString("value");
                }
            }
            
            // srt_cn_local
            JSONObject jsonSrtCnLocal = object.getJSONObject("field_ted_talk_srt_cn_local");
            JSONArray arrSrtCnLocal = jsonSrtCnLocal.getJSONArray("und");
            for(int i = 0; i < arrSrtCnLocal.length(); i++) {
                JSONObject obj = arrSrtCnLocal.getJSONObject(i);
                if(obj.has("value")) {
                    srtCnLocal = obj.getString("value");
                }
            }
            
            // category
            JSONObject jsonCategory = object.getJSONObject("field_ted_talk_category");
            JSONArray arrCategory = jsonCategory.getJSONArray("und");
            JSONObject targetCategory = null;
            for(int i = 0; i < arrCategory.length(); i++) {
                targetCategory = arrCategory.getJSONObject(i);
                if(targetCategory.has("tid")) {
                    switch (i) {
                    case 0:
                        category1 = Integer.parseInt(targetCategory.getString("tid"));
                        break;
                        
                    case 1:
                        category2 = Integer.parseInt(targetCategory.getString("tid"));
                        break;
    
                    default:
                        break;
                    }
                }
                targetCategory = null;
            }
            
            ELearningDb db = new ELearningDb(mContext);
            db.open();
            switch (mOperation) {
            case INSERT:
                db.insertTedTalksRow(
                        nid, tid, title, body, author1, author2, image, video, mp3, mp3Local, srt, 
                        srtLocal, srtCn, srtCnLocal, videoLd, videoLdLocal, videoSd, videoSdLocal, 
                        videoHd, videoHdLocal, category1, category2);
                break;
                
            case UPDATE:
                db.updateTedTalksRow(
                        nid, tid, title, body, author1, author2, image, video, mp3, mp3Local, srt, 
                        srtLocal, srtCn, srtCnLocal, videoLd, videoLdLocal, videoSd, videoSdLocal, 
                        videoHd, videoHdLocal, category1, category2);
                break;

            default:
                break;
            }
            db.close();
            
            
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void saveToECornerSpeakersTable(String jsonString) {
        int nid = 0, tid = 0;
        String title = "", body = "", image = "", company = "", www = "";
        
        try {
            JSONObject object = new JSONObject(jsonString);
            
            // nid
            nid = object.getInt("nid");
            
            // tid
    
            // title
            title = object.getString("title");
            
            // body
            JSONObject jsonBody = object.getJSONObject("body");
            JSONArray arrBody = jsonBody.getJSONArray("und");
            for(int i = 0; i < arrBody.length(); i++) {
                JSONObject obj = arrBody.getJSONObject(i);
                if(obj.has("value")) {
                    body = obj.getString("value");
                }
            }
            
            // image
            JSONObject jsonImage = object.getJSONObject("field_ecorner_speaker_image");
            JSONArray arrImage = jsonImage.getJSONArray("und");
            for(int i = 0; i < arrImage.length(); i++) {
                JSONObject obj = arrImage.getJSONObject(i);
                if(obj.has("filename")) {
                    image = obj.getString("filename");
                }
            }
            
            // company
            JSONObject jsonCompany = object.getJSONObject("field_ecorner_speaker_company");
            JSONArray arrCompany = jsonCompany.getJSONArray("und");
            for(int i = 0; i < arrCompany.length(); i++) {
                JSONObject obj = arrCompany.getJSONObject(i);
                if(obj.has("value")) {
                    company = obj.getString("value");
                }
            }
            
            // www
            JSONObject jsonWww = object.getJSONObject("field_ecorner_speaker_www");
            JSONArray arrWww = jsonWww.getJSONArray("und");
            for(int i = 0; i < arrWww.length(); i++) {
                JSONObject obj = arrWww.getJSONObject(i);
                if(obj.has("url")) {
                    www = obj.getString("url");
                }
            }
            
            ELearningDb db = new ELearningDb(mContext);
            db.open();
            
            switch (mOperation) {
            case INSERT:
                db.insertECornerSpeakersRow(nid, tid, title, body, image, company, www);
                break;

            case UPDATE:
                db.updateECornerSpeakersRow(nid, tid, title, body, image, company, www);
                break;
                
            default:
                break;
            }
            db.close();
            
            
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void saveToECornerVideoTable(String jsonString) {
        int nid = 0, tid = 0, author1 = 0, author2 = 0, category1 = 0, category2 = 0;
        String title = "", body = "", image = "",
               video = "", videoLocal = "",
               srt = "", srtLocal = "",
               srtCn = "", srtCnLocal = "",
               mp3 = "", mp3Local = "";
        
        try {
            JSONObject object = new JSONObject(jsonString);
            
            // nid
            nid = object.getInt("nid");
            
            // tid
    
            // title
            title = object.getString("title");
            
            // body
            JSONObject jsonBody = object.getJSONObject("body");
            JSONArray arrBody = jsonBody.getJSONArray("und");
            for(int i = 0; i < arrBody.length(); i++) {
                JSONObject obj = arrBody.getJSONObject(i);
                if(obj.has("value")) {
                    body = obj.getString("value");
                }
            }
            
            // image
            JSONObject jsonImage = object.getJSONObject("field_ecorner_image");
            JSONArray arrImage = jsonImage.getJSONArray("und");
            for(int i = 0; i < arrImage.length(); i++) {
                JSONObject obj = arrImage.getJSONObject(i);
                if(obj.has("filename")) {
                    image = obj.getString("filename");
                }
            }
            
            // author
            JSONObject jsonAuthor = object.getJSONObject("field_ecorner_author");
            JSONArray arrAuthor = jsonAuthor.getJSONArray("und");
            JSONObject targetAuthor = null;
            for(int i = 0; i < arrAuthor.length(); i++) {
                targetAuthor = arrAuthor.getJSONObject(i);
                if(targetAuthor.has("nid")) {
                    switch (i) {
                    case 0:
                        author1 = Integer.parseInt(targetAuthor.getString("nid"));
                        break;
                        
                    case 1:
                        author2 = Integer.parseInt(targetAuthor.getString("nid"));
                        break;
    
                    default:
                        break;
                    }
                }
                targetAuthor = null;
            }
            
            // category
            JSONObject jsonCategory = object.getJSONObject("field_ecorner_video_category");
            JSONArray arrCategory = jsonCategory.getJSONArray("und");
            JSONObject targetCategory = null;
            for(int i = 0; i < arrCategory.length(); i++) {
                targetCategory = arrCategory.getJSONObject(i);
                if(targetCategory.has("tid")) {
                    switch (i) {
                    case 0:
                        category1 = Integer.parseInt(targetCategory.getString("tid"));
                        break;
                        
                    case 1:
                        category2 = Integer.parseInt(targetCategory.getString("tid"));
                        break;
    
                    default:
                        break;
                    }
                }
                targetCategory = null;
            }
            
            // srt
            JSONObject jsonSrt = object.getJSONObject("field_ecorner_srt");
            JSONArray arrSrt = jsonSrt.getJSONArray("und");
            for(int i = 0; i < arrSrt.length(); i++) {
                JSONObject obj = arrSrt.getJSONObject(i);
                if(obj.has("value")) {
                    srt = obj.getString("value");
                }
            }
            
            // srt_local
            JSONObject jsonSrtLocal = object.getJSONObject("field_ecorner_srt_local");
            JSONArray arrSrtLocal = jsonSrtLocal.getJSONArray("und");
            for(int i = 0; i < arrSrtLocal.length(); i++) {
                JSONObject obj = arrSrtLocal.getJSONObject(i);
                if(obj.has("value")) {
                    srtLocal = obj.getString("value");
                }
            }
            
            // srt_cn
            JSONObject jsonSrtCn = object.getJSONObject("field_ecorner_srt_cn");
            JSONArray arrSrtCn = jsonSrtCn.getJSONArray("und");
            for(int i = 0; i < arrSrtCn.length(); i++) {
                JSONObject obj = arrSrtCn.getJSONObject(i);
                if(obj.has("value")) {
                    srtCn = obj.getString("value");
                }
            }
            
            // srt_cn_local
            JSONObject jsonSrtCnLocal = object.getJSONObject("field_ecorner_srt_cn_local");
            JSONArray arrSrtCnLocal = jsonSrtCnLocal.getJSONArray("und");
            for(int i = 0; i < arrSrtCnLocal.length(); i++) {
                JSONObject obj = arrSrtCnLocal.getJSONObject(i);
                if(obj.has("value")) {
                    srtCnLocal = obj.getString("value");
                }
            }
            
            // mp3
            JSONObject jsonMp3 = object.getJSONObject("field_ecorner_mp3");
            JSONArray arrMp3 = jsonMp3.getJSONArray("und");
            for(int i = 0; i < arrMp3.length(); i++) {
                JSONObject obj = arrMp3.getJSONObject(i);
                if(obj.has("value")) {
                    mp3 = obj.getString("value");
                }
            }
            
            // mp3_local
            JSONObject jsonMp3Local = object.getJSONObject("field_ecorner_mp3_local");
            JSONArray arrMp3Local = jsonMp3Local.getJSONArray("und");
            for(int i = 0; i < arrMp3Local.length(); i++) {
                JSONObject obj = arrMp3Local.getJSONObject(i);
                if(obj.has("value")) {
                    mp3Local = obj.getString("value");
                }
            }
            
            // video
            JSONObject jsonVideo = object.getJSONObject("field_ecorner_video");
            JSONArray arrVideo = jsonVideo.getJSONArray("und");
            for(int i = 0; i < arrVideo.length(); i++) {
                JSONObject obj = arrVideo.getJSONObject(i);
                if(obj.has("value")) {
                    video = obj.getString("value");
                }
            }
            
            // video_local
            JSONObject jsonVideoLocal = object.getJSONObject("field_ecorner_video_local");
            JSONArray arrVideoLocal = jsonVideoLocal.getJSONArray("und");
            for(int i = 0; i < arrVideoLocal.length(); i++) {
                JSONObject obj = arrVideoLocal.getJSONObject(i);
                if(obj.has("value")) {
                    videoLocal = obj.getString("value");
                }
            }
            
            ELearningDb db = new ELearningDb(mContext);
            db.open();
            
            switch (mOperation) {
            case INSERT:
                db.insertECornerVideoTable(nid, tid, title, body, author1, author2, category1, 
                        category2, image, video, videoLocal, srt, srtLocal, srtCn, srtCnLocal, 
                        mp3, mp3Local);
                break;
                
            case UPDATE:
                db.updateECornerVideoTable(nid, tid, title, body, author1, author2, category1, 
                        category2, image, video, videoLocal, srt, srtLocal, srtCn, srtCnLocal, 
                        mp3, mp3Local);
                break;

            default:
                break;
            }
            
            db.close();
            
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void setContext(Context context) {
        mContext = context;
    }
}
