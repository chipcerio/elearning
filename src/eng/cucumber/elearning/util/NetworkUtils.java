/*
 * Copyright (C) 2013 Alistair Cerio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eng.cucumber.elearning.util;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

/**
 * Assortment of Network helpers
 */
public class NetworkUtils {

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        
        if (activeNetwork == null) {
            if (DebugLog.DEBUG_ON) {
                // comment this Toast if you execute HttpJsonParserActivity
                Toast.makeText(context, "No internet connection", Toast.LENGTH_LONG).show();
            }
            return false;
            
        } else {
            if (DebugLog.DEBUG_ON) {
                // comment this Toast if you execute HttpJsonParserActivity
                Toast.makeText(context, "has internet connection", Toast.LENGTH_LONG).show();
            }
            return activeNetwork.isConnectedOrConnecting();
        }
        
    }
}
