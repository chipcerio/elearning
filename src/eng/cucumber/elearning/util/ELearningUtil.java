package eng.cucumber.elearning.util;
/*
 * Copyright (C) 2013 Alistair Cerio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.annotation.SuppressLint;

/**
 * A general class of utilities
 */
public class ELearningUtil {

    public static final int TED_VIDEO = 1;
    public static final int ECORNER_VIDEO = 2;

    @SuppressLint("SimpleDateFormat")
    public static String getCurrentDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        return dateFormat.format(date);
    }

    /**
     * Generates MD5 hash algorithm from a given String.
     * Source: <a href="http://stackoverflow.com/a/4846511/1076574">StackOverflow</a>
     * 
     * @param msg String to be hashed
     * @return hashed password
     */
    public static final String md5(String msg) {
        try {
            // create md5 hash
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(msg.getBytes());
            byte messageDigest[] = md.digest();

            // create hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++) {
                String h = Integer.toHexString(0xFF & messageDigest[i]);
                while (h.length() < 2) {
                    h = "0" + h;
                }
                hexString.append(h);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return "";
    }

    /**
     * Generates a random alpha-numeric String. This is a little bit more expensive.
     * Note that SecureRandom objects are expensive to initialize, so you'll want to keep
     * one around and use it.
     * Source: <a href="http://stackoverflow.com/a/41156/1076574">StackOverflow</a>
     * 
     * @return alpha-numeric String
     */
    public static String generateSalt() {
        SecureRandom random = new SecureRandom();
        BigInteger bigInt = new BigInteger(130, random);
        return bigInt.toString(32);
    }
}
