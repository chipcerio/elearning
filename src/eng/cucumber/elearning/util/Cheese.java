/*
 * Copyright (C) 2013 Alistair Cerio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eng.cucumber.elearning.util;

import eng.cucumber.elearning.R;
import eng.cucumber.elearning.app.CourseFragment;
import eng.cucumber.elearning.app.InstructorFragment;
import eng.cucumber.elearning.app.UniversityFragment;

public class Cheese {

    // public static final String BASE_URL = "http://192.168.1.33"; // wifi
    // private static final String BASE_URL = "http://10.0.2.2";  // emulator
    public static final String BASE_URL = "http://ithubtrading.com"; // live

    // localhost
    public static final String SVC_URL = BASE_URL + "/d7coursera/my_services/node/";
    public static final String IMG_URL = BASE_URL + "/d7coursera/sites/default/files/";
    
    public static final String VIDEO_DIR = "/d7coursera/media/";

    // http://localhost/d7coursera/has-update/2012-09-11--2012-09-12?response_type=json
    public static final String HAS_UPDATE_URL = BASE_URL + "/d7coursera/has-update/";

    // http://localhost/d7coursera/content-last-modified/20130213--20130311/ted_talks?response_type=json
    public static final String CONTENT_LAST_MODIFIED_URL = BASE_URL + "/d7coursera/content-last-modified/";

    // ?response_type=json
    public static final String FORMAT_JSON = "?response_type=json";


    public static final String DOWNLOAD_DIR = "/elearning/";

    public static final String[] ITEMS = {
        "Item 1", "Item 2", "Item 3", "Item 4", "Item 5",
        "Item 6", "Item 7", "Item 8", "Item 9", "Item 10",
        "Item 11", "Item 12", "Item 13", "Item 14", "Item 15",
        "Item 16", "Item 17", "Item 18", "Item 19", "Item 20"
    };

    public static final String[] COURSES = {
        "All", "Computer", "Economics", "Healthcare", "Humanities",
        "Languages", "Mathematics", "Society"
    };

    public static final String[] UNIVERSITIES = {
        "All", "eCorner Stanford", "EngVid.com", "Khan Academy", "Princeton",
        "Stanford", "TED Talk", "California - Berkeley", "Michigan", "Pennsylvania"
    };

    public static final String[] LECTURERS = {
        "All", "Computer", "Economics", "Healthcare", "Humanities",
        "Languages", "Mathematics", "Society"
    };

    public static final String[] CATEGORIES = {
        "Society, Networks, and Information",
        "Mathematics and Statistics",
        "Healthcare",
        "Computer Science",
        "Humanities and Social Sciences",
        "Economics, Finance, and Business"
    };

    public static Integer[] IMAGES = {
        R.drawable.img_01_networks,
        R.drawable.img_02_math,
        R.drawable.img_03_health,
        R.drawable.img_04_cs,
        R.drawable.img_05_humanities,
        R.drawable.img_06_economics
    };

    public static final String PAGE1 = CourseFragment.class.getName();
    public static final String PAGE2 = UniversityFragment.class.getName();
    public static final String PAGE3 = InstructorFragment.class.getName();

}
