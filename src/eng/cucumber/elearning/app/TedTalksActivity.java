package eng.cucumber.elearning.app;
/*
 * Copyright (C) 2013 Alistair Cerio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static eng.cucumber.elearning.util.Cheese.DOWNLOAD_DIR;
import static eng.cucumber.elearning.util.Cheese.IMG_URL;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import eng.cucumber.elearning.R;
import eng.cucumber.elearning.database.ELearningData;

public class TedTalksActivity extends SherlockActivity implements OnItemClickListener {
    private GridView mGrid;
    private ELearningData mDb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_ted_talks);

        mDb = new ELearningData(this);

        mGrid = (GridView) findViewById(R.id.grid_ted_talks);
        mGrid.setAdapter(new ImageAdapter(this));
        mGrid.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(this, "position=" + position, Toast.LENGTH_SHORT).show();
        Intent i = new Intent(this, VideoInfoActivity.class);
        i.putExtra("position", position + 1);
        i.putExtra("type", DOWNLOAD_DIR);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.a_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }

    class ImageAdapter extends BaseAdapter {
        private Context mContext;
        private LayoutInflater mInflater;

        public ImageAdapter(Context context) {
            mContext = context;
            mInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return mDb.getTedTalks().size();
        }

        @Override
        public Object getItem(int position) {
            return mDb.getTedTalks().get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.item_ted_talks, parent, false);
                holder = new ViewHolder();
                holder.image = (ImageView) convertView.findViewById(R.id.img_item_ted_talks);
                holder.text  = (TextView)  convertView.findViewById(R.id.txt_item_ted_talks);
                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            String img = mDb.getTedTalksImage(position + 1).replaceAll("\\s+", "%20");
            UrlImageViewHelper.setUrlDrawable(holder.image, IMG_URL + img, R.drawable.image_placeholder);
            holder.text.setText(mDb.getTedTalks().get(position));

            return convertView;
        }
    }

    static class ViewHolder {
        ImageView image;
        TextView text;
    }

}