package eng.cucumber.elearning.app;
/*
 * Copyright (C) 2013 Alistair Cerio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static eng.cucumber.elearning.util.Cheese.BASE_URL;
import static eng.cucumber.elearning.util.Cheese.VIDEO_DIR;

import java.io.File;

import android.os.Bundle;
import android.os.Environment;
import android.widget.MediaController;
import android.widget.VideoView;

import com.actionbarsherlock.app.SherlockActivity;

import eng.cucumber.elearning.R;
import eng.cucumber.elearning.database.ELearningData;
import eng.cucumber.elearning.util.ELearningUtil;

public class VideoActivity extends SherlockActivity {
    private int mSelected;
    private int mVideoType;
    private VideoView mVideo;
    private MediaController mController;
    private ELearningData mDb;
    private String mDownloadDir;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_video);
        mSelected = getIntent().getIntExtra("selected", 0);
        mDownloadDir = getIntent().getStringExtra("type");
        mVideoType = getIntent().getIntExtra("video", 0);

        mDb = new ELearningData(this);

        mVideo = (VideoView) findViewById(R.id.video);
        mController = new MediaController(this);

        switch (mVideoType) {
            case ELearningUtil.TED_VIDEO:
                playTedVideo();
                break;

            case ELearningUtil.ECORNER_VIDEO:
                playECornerVideo();
                break;

            default:
                break;
        }

        mController.setMediaPlayer(mVideo);
        mVideo.setMediaController(mController);
        mVideo.requestFocus();
        mVideo.start();
    }

    private void playTedVideo() {
        File clip = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath(), mDownloadDir + "/"+ mDb.getTedSdVideoForLocal(mSelected));
        if (clip.exists()) {
            mVideo.setVideoPath(clip.getAbsolutePath()); // play from sd

        } else {
            mVideo.setVideoPath(BASE_URL + VIDEO_DIR + mDb.getTedSdVideoUrl(mSelected)); //stream
        }
    }

    private void playECornerVideo() {
        File clip = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath(), mDownloadDir + "/"+ mDb.getECornerSdVideoForLocal(mSelected).replaceAll("\\s+", ""));
        if (clip.exists()) {
            mVideo.setVideoPath(clip.getAbsolutePath()); // play from sd

        } else {
            String url = mDb.getECornerSdVideoUrl(mSelected).replaceAll("\\s+", "%20");
            mVideo.setVideoPath(url); // TODO stream
        }
    }

}
