package eng.cucumber.elearning.app;

import static eng.cucumber.elearning.util.Cheese.IMG_URL;

import java.util.List;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import eng.cucumber.elearning.R;
import eng.cucumber.elearning.database.ELearningData;
import eng.cucumber.elearning.util.DBUtils.CourseColumns;
import eng.cucumber.elearning.util.DebugLog;

public class SelectCourseFragment extends SherlockFragment implements OnItemClickListener {
    private GridView mGrid;
    private ELearningData mDb;
    private List<String> mImageList;
    private List<String> mTitleList;
    private static int sCategory;
    
    public static SelectCourseFragment newInstance(int index) {
        SelectCourseFragment fragment = new SelectCourseFragment();
        sCategory = index;
        
        Bundle args = new Bundle();
        args.putInt("index", index);
        fragment.setArguments(args);
        
        return fragment;
    }
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle cucumber) {
        View root = inflater.inflate(R.layout.f_select_course, container, false);
        
        mGrid = (GridView) root.findViewById(R.id.grid_select_course);
        mDb = ((MainActivity2) getActivity()).mDb;
        mImageList = mDb.getSubCategoryImages(sCategory);
        mTitleList = mDb.getSubCategoryTitles(sCategory);
        
        return root;
    }
    
    @Override
    public void onActivityCreated(Bundle cucumber) {
        super.onActivityCreated(cucumber);
        mGrid.setAdapter(new ImageAdapter(getActivity()));
        mGrid.setOnItemClickListener(this);
    }
    
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String title = mTitleList.get(position);
        
        DebugLog.d(TAG, "WHERE CLAUSE RESULT >>>>>>>> " 
        + mDb.getCourseOutlineByTitle(title, CourseColumns.COURSE_BODY));
        
        Fragment fragment = CourseOutlineFragment.newInstance(title);
        FragmentTransaction ft = getSherlockActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container_fragment_details, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(null);
        ft.commit();
    }

    private static final String TAG = "SelectCourseFragment";
    
    class ImageAdapter extends BaseAdapter {
        private int mSize;
        private Context mContext;
        private LayoutInflater mInflater;
        
        public ImageAdapter(Context context) {
            mSize = mTitleList.size();
            mContext = context;
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return mSize;
        }

        @Override
        public Object getItem(int arg0) {
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            return 0;
        }

        @SuppressWarnings("static-access")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.r_item_subcategory, null);
                holder = new ViewHolder();
                
                holder.image = (ImageView) convertView.findViewById(R.id.img_grid_item);
                holder.text  = (TextView)  convertView.findViewById(R.id.txt_grid_caption);
                convertView.setTag(holder);
                
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            
            String photo = mImageList.get(position);
            photo = photo.replaceAll("\\s+", "%20");
            UrlImageViewHelper.setUrlDrawable(holder.image, IMG_URL + photo, R.drawable.img_03_health);
            holder.text.setText(mTitleList.get(position));
            
            return convertView;
        }
        
    }
    
    static class ViewHolder {
        static ImageView image;
        static TextView text;
    }
}
