package eng.cucumber.elearning.app;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;

import eng.cucumber.elearning.R;
import eng.cucumber.elearning.database.ELearningData;
import eng.cucumber.elearning.util.DBUtils.ClassGroupColumns;

public class CourseGroupFragment extends SherlockFragment {
    private static String sTitle;
    private TextView mBody;
    private ELearningData mDb;
    
    public static CourseGroupFragment newInstance(String title) {
        CourseGroupFragment fragment = new CourseGroupFragment();
        
        sTitle = title;
        Bundle args = new Bundle();
        args.putString("title", title);
        fragment.setArguments(args);
        
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle cucumber) {
        View root = inflater.inflate(R.layout.f_course_group, container, false);
        
        mBody = (TextView) root.findViewById(R.id.txt_course_group_body);
        mDb = new ELearningData(getActivity());
        
        return root;
    }
    
    @Override
    public void onActivityCreated(Bundle cucumber) {
        super.onActivityCreated(cucumber);
        mBody.setText(Html.fromHtml(mDb.getClassGroup(sTitle, ClassGroupColumns.CLASS_GROUP_BODY)));
    }
}
