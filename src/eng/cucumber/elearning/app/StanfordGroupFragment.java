package eng.cucumber.elearning.app;

import static eng.cucumber.elearning.util.Cheese.DOWNLOAD_DIR;
import static eng.cucumber.elearning.util.Cheese.IMG_URL;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import eng.cucumber.elearning.R;
import eng.cucumber.elearning.database.ELearningData;
import eng.cucumber.elearning.util.DBUtils.ECornerVideoColumns;
import eng.cucumber.elearning.util.DebugLog;
import eng.cucumber.elearning.util.ELearningUtil;

public class StanfordGroupFragment extends SherlockFragment implements OnItemClickListener {
    private static String sGroup;
    private GridView mGrid;
    private ELearningData mDb;

    public static StanfordGroupFragment newInstance(String group) {
        StanfordGroupFragment fragment = new StanfordGroupFragment();
        sGroup = group;

        Bundle args = new Bundle();
        args.putString("group", group);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle peanut) {
        View root = inflater.inflate(R.layout.f_video_group, container, false);

        mDb = ((MainActivity2) getActivity()).mDb;
        mGrid = (GridView) root.findViewById(R.id.grid_video_group);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle peanut) {
        super.onActivityCreated(peanut);
        mGrid.setAdapter(new ImageAdapter(getActivity()));
        mGrid.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        DebugLog.d(TedTalksGroupFragment.class.getName(), "selected=" + position);

        int selected = Integer.parseInt(mDb.getStanfordGroup(sGroup, ECornerVideoColumns.ECORNER_VIDEO_ID)
                .get(position));
        Intent i = new Intent(getActivity(), VideoInfoActivity.class);
        i.putExtra("position", selected);
        i.putExtra("type", DOWNLOAD_DIR);
        i.putExtra("video", ELearningUtil.ECORNER_VIDEO);
        startActivity(i);
    }

    class ImageAdapter extends BaseAdapter {
        private Context mContext;
        private LayoutInflater mInflater;

        public ImageAdapter(Context context) {
            mContext = context;
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            return mDb.getStanfordGroupSize(sGroup);
        }

        @Override
        public Object getItem(int position) {
            return mDb.getStanfordGroup(sGroup, ECornerVideoColumns.ECORNER_VIDEO_GROUP)
                    .get(position);
        }

        @Override
        public long getItemId(int arg0) {
            return 0;
        }

        @SuppressWarnings("static-access")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.r_item_ecorner_video, parent, false);
                holder = new ViewHolder();
                holder.image = (ImageView) convertView.findViewById(R.id.img_grid_item_ecorner);
                holder.text  = (TextView)  convertView.findViewById(R.id.txt_grid_caption_ecorner);

                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            String img = mDb.getStanfordGroup(sGroup, ECornerVideoColumns.ECORNER_VIDEO_IMAGE)
                    .get(position).replaceAll("\\s+", "%20");
            DebugLog.d(StanfordGroupFragment.class.getName(), "url=" + IMG_URL + img);
            UrlImageViewHelper.setUrlDrawable(holder.image, IMG_URL + img, R.drawable.image_placeholder);

            String title = mDb.getStanfordGroup(sGroup, ECornerVideoColumns.ECORNER_VIDEO_TITLE)
                    .get(position);
            holder.text.setText(title);

            return convertView;
        }

    }

    static class ViewHolder {
        static ImageView image;
        static TextView text;
    }

}
