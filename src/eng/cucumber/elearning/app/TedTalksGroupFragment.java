package eng.cucumber.elearning.app;

import static eng.cucumber.elearning.util.Cheese.DOWNLOAD_DIR;
import static eng.cucumber.elearning.util.Cheese.IMG_URL;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import eng.cucumber.elearning.R;
import eng.cucumber.elearning.database.ELearningData;
import eng.cucumber.elearning.util.DBUtils.TedTalksColumns;
import eng.cucumber.elearning.util.DebugLog;
import eng.cucumber.elearning.util.ELearningUtil;

public class TedTalksGroupFragment extends SherlockFragment implements OnItemClickListener {
    private static String sGroup;
    private GridView mGrid;
    private ELearningData mDb;

    public static TedTalksGroupFragment newInstance(String group) {
        TedTalksGroupFragment fragment = new TedTalksGroupFragment();
        sGroup = group;

        Bundle args = new Bundle();
        args.putString("group", group);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle cucumber) {
        View root = inflater.inflate(R.layout.f_video_group, container, false);

        mDb = ((MainActivity2) getActivity()).mDb;
        mGrid = (GridView) root.findViewById(R.id.grid_video_group);

        return root;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mGrid.setAdapter(new ImageAdapter(getActivity()));
        mGrid.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        DebugLog.d(TedTalksGroupFragment.class.getName(), "selected=" + position);

        int selected = Integer.parseInt(mDb.getTedTalksGroup(sGroup, TedTalksColumns.TED_TALKS_ID)
                .get(position));
        Intent i = new Intent(getActivity(), VideoInfoActivity.class);
        i.putExtra("position", selected);
        i.putExtra("type", DOWNLOAD_DIR);
        i.putExtra("video", ELearningUtil.TED_VIDEO);
        startActivity(i);
    }

    class ImageAdapter extends BaseAdapter {
        private Context mContext;
        private LayoutInflater mInflater;

        public ImageAdapter(Context context) {
            mContext = context;
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            DebugLog.d(TedTalksGroupFragment.class.getName(), "size=" + mDb.getTedTalksGroupSize(sGroup));
            return mDb.getTedTalksGroupSize(sGroup);
        }

        @Override
        public Object getItem(int position) {
            return mDb.getTedTalksGroup(sGroup, TedTalksColumns.TED_TALKS_IMAGE)
                    .get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @SuppressWarnings("static-access")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.r_item_ted_video, parent, false);
                holder = new ViewHolder();
                holder.image = (ImageView) convertView.findViewById(R.id.img_grid_item_ted);
                holder.text  = (TextView)  convertView.findViewById(R.id.txt_grid_caption_ted);

                convertView.setTag(holder);

            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            String img = mDb.getTedTalksGroup(sGroup, TedTalksColumns.TED_TALKS_IMAGE)
                    .get(position).replaceAll("\\s+", "%20");
            DebugLog.d(TedTalksGroupFragment.class.getName(), "url=" + IMG_URL + img);
            UrlImageViewHelper.setUrlDrawable(holder.image, IMG_URL + img, R.drawable.image_placeholder);
            holder.text.setText("");

            return convertView;
        }

    }

    static class ViewHolder {
        static ImageView image;
        static TextView text;
    }
}
