package eng.cucumber.elearning.app;

import android.app.Application;
import android.content.SharedPreferences;

public class ELearningApp extends Application {
    
    private static SharedPreferences sPref;
    private static ELearningApp sInstance;
    
    private static final String USER_PREF = "user_pref";
    private static final String HAS_USER = "has_user";
    
    public ELearningApp() {
        super();
        sInstance = this;
    }
    
    @Override
    public void onCreate() {
        super.onCreate();
    }
    
    public static ELearningApp getInstance() {
        if (sInstance == null) {
            synchronized (ELearningApp.class) {
                if (sInstance == null) new ELearningApp();
            }
        }
        
        if (sPref == null) {
            sPref = sInstance.getSharedPreferences(USER_PREF, MODE_PRIVATE);
        }
        
        return sInstance;
    }

    /**
     * Sets the app when user exists
     * 
     * @param hasUser true when user exists otherwise false
     */
    public void setUserFlag(boolean hasUser) {
        SharedPreferences.Editor editor = sPref.edit();
        editor.putBoolean(HAS_USER, hasUser);
        editor.commit();
    }
    
    /**
     * Identifies if there is/are no existing user
     * 
     * @return true if there are users otherwise false 
     */
    public boolean hasUser() {
        return sPref.getBoolean(HAS_USER, false);
    }
}
