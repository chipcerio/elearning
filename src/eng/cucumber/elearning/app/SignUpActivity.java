package eng.cucumber.elearning.app;
/*
 * Copyright (C) 2013 Alistair Cerio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

import eng.cucumber.elearning.R;
import eng.cucumber.elearning.database.ELearningData;
import eng.cucumber.elearning.database.ELearningDb;
import eng.cucumber.elearning.util.DBUtils.UserColumns;
import eng.cucumber.elearning.util.ELearningUtil;

public class SignUpActivity extends SherlockActivity {
    private ELearningData mDb;
    private EditText mUsername;
    private EditText mPassword;
    private EditText mRepeatPassword;
    private EditText mEmail;
    
    @Override
    protected void onCreate(Bundle cucumber) {
        super.onCreate(cucumber);
        setContentView(R.layout.a_signup);
        
        mDb = new ELearningData(this);
        
        mUsername = (EditText) findViewById(R.id.edittext_signup_username);
        mPassword = (EditText) findViewById(R.id.edittext_signup_password);
        mRepeatPassword = (EditText) findViewById(R.id.edittext_signup_repeat_password);
        mEmail = (EditText) findViewById(R.id.edittext_signup_email);
    }
    
    public void signUpClick(View view) {
        // check if passwords match
        boolean match = mPassword.getText().toString().contentEquals(
                mRepeatPassword.getText().toString());
        if (match) {
            checkUserName(mUsername.getText().toString());
        } else {
            Toast.makeText(this, "Password didn't match", Toast.LENGTH_LONG).show();
        }
    }
    
    private void checkUserName(String username) {
        // check username in the database if exists
        if (mDb.search(username, UserColumns.USER_NAME)) {
            Toast.makeText(this, "User already exists", Toast.LENGTH_LONG).show();
        } else {
            checkEmail(mEmail.getText().toString());
        }
    }
    
    private void checkEmail(String email) {
        // check email in the database if exists
        if (mDb.search(email, UserColumns.USER_EMAIL)) {
            Toast.makeText(this, "E-Mail already exists", Toast.LENGTH_LONG).show();
        } else {
            // proceed with storing to database
            // TODO email validation http://stackoverflow.com/a/624590/1076574
            String salt = ELearningUtil.generateSalt();
            String hash = ELearningUtil.md5(mPassword.getText().toString() + salt);
            
            ELearningDb db = new ELearningDb(this);
            db.open();
            db.insertUser(mUsername.getText().toString(), hash, salt, mEmail.getText().toString());
            db.close();
            
            ELearningApp.getInstance().setUserFlag(true);
            
            Intent i = new Intent(this, MainActivity2.class);
            startActivity(i);
            finish();
        }
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

}
