package eng.cucumber.elearning.app;
/*
 * Copyright (C) 2013 Alistair Cerio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

import eng.cucumber.elearning.R;
import eng.cucumber.elearning.database.ELearningData;
import eng.cucumber.elearning.util.DBUtils.UserColumns;
import eng.cucumber.elearning.util.DebugLog;
import eng.cucumber.elearning.util.ELearningUtil;

public class LoginActivity extends SherlockActivity {
    private ELearningData mAppDb;
    private EditText mUser;
    private EditText mPass;
    
    @Override
    protected void onCreate(Bundle cucumber) {
        super.onCreate(cucumber);
        setContentView(R.layout.a_login);
        mAppDb = new ELearningData(this);
        
        mUser = (EditText) findViewById(R.id.edittext_login_username);
        mPass = (EditText) findViewById(R.id.edittext_login_password);
        
        if (DebugLog.DEBUG_ON) {
            mUser.setText("u01");
            mPass.setText("u01password");
        }
        
        ELearningApp.getInstance().setUserFlag(true);
    }
    
    public void loginClick(View view) {
        boolean hasUser = ELearningApp.getInstance().hasUser();
        if (hasUser) {
            loginUser(mUser.getText().toString().trim());
            
        } else { // TODO check remote if user exists
            ;
        }
    }
    
    private void loginUser(String userName) {
        if (mAppDb.search(userName, UserColumns.USER_NAME)) {
            // adding salt to password
            String pass = mPass.getText().toString() + mAppDb.getSalt(userName);
            String hash = ELearningUtil.md5(pass);
            
            // compare the hashes
            if (hash.contentEquals(mAppDb.getHashedPassword(userName))) {
                Intent i = new Intent(this, MainActivity2.class);
                startActivity(i);
                finish();
            }
            
        } else {
            Toast.makeText(this, "Invalid Username or Password", Toast.LENGTH_LONG).show();
        }
    }
    
    public void signupClick(View view) {
        Intent i = new Intent(this, SignUpActivity.class);
        startActivity(i);
        finish();
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }
}
