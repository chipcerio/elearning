/*
 * Copyright (C) 2013 Alistair Cerio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eng.cucumber.elearning.app;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.actionbarsherlock.app.SherlockFragment;

import eng.cucumber.elearning.R;
import eng.cucumber.elearning.database.ELearningData;

public class UniversityDetailFragment extends SherlockFragment {
    private static int sIndex;
    private ELearningData mDb;
    
    public static UniversityDetailFragment newInstance(int index) {
        UniversityDetailFragment f = new UniversityDetailFragment();
        sIndex = index + 1;
        
        Bundle args = new Bundle();
        args.putInt("index", index);
        f.setArguments(args);
        
        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle icicle) {
        View root = inflater.inflate(R.layout.f_university_detail, container, false);
        mDb = ((BaseActivity) getActivity()).mAppDb;
        Button b = (Button) root.findViewById(R.id.btn_university);
        b.setText(Html.fromHtml(mDb.getUniversityDetail(sIndex)));
        return root;
    }

}
