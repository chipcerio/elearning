package eng.cucumber.elearning.app;

import static eng.cucumber.elearning.util.Cheese.IMG_URL;

import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import eng.cucumber.elearning.R;
import eng.cucumber.elearning.database.ELearningData;
import eng.cucumber.elearning.util.DBUtils.CourseColumns;
import eng.cucumber.elearning.util.DBUtils.InstructorColumns;

public class CourseOutlineFragment extends SherlockFragment {
    private static String sTitle;
    private TextView mTitle;
    private TextView mBody;
    private TextView mInstructorTitle;
    private TextView mInstructorBody;
    private ImageView mImage;
    private ImageView mInstructorImage;
    private ELearningData mDb;
    
    public static CourseOutlineFragment newInstance(String title) {
        CourseOutlineFragment fragment = new CourseOutlineFragment();
        sTitle = title;
        
        Bundle args = new Bundle();
        args.putString("title", title);
        fragment.setArguments(args);
        
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle peanut) {
        View root = inflater.inflate(R.layout.f_course_outline, container, false);
        
        mDb = new ELearningData(getActivity());
        
        mTitle = (TextView) root.findViewById(R.id.text_course_outline_title);
        mBody = (TextView) root.findViewById(R.id.text_course_outline_body);
        mInstructorTitle = (TextView) root.findViewById(R.id.text_course_outline_instructor_title);
        mInstructorBody = (TextView) root.findViewById(R.id.text_course_outline_instructor_body);
        mImage = (ImageView) root.findViewById(R.id.img_course_outline);
        mInstructorImage = (ImageView) root.findViewById(R.id.img_course_outline_instructor);
        
        return root;
    }
    
    @Override
    public void onActivityCreated(Bundle cucumber) {
        super.onActivityCreated(cucumber);
        
        // course
        mTitle.setText(sTitle);
        
        String courseImage = mDb.getCourseOutlineByTitle(sTitle, CourseColumns.COURSE_IMAGE);
        courseImage = courseImage.replace(" ", "%20"); // "\\s+" 
        UrlImageViewHelper.setUrlDrawable(mImage, 
                IMG_URL + courseImage, R.drawable.image_placeholder);
        
        mBody.setText(Html.fromHtml(mDb.getCourseOutlineByTitle(sTitle, 
                CourseColumns.COURSE_BODY)));
        
        // instructor
        mInstructorTitle.setText(mDb.getCourseOutlineByTitle(sTitle, 
                InstructorColumns.INSTRUCTOR_TITLE));
        
        String instructorImage = mDb.getCourseOutlineByTitle(sTitle, 
                InstructorColumns.INSTRUCTOR_IMAGE);
        instructorImage = instructorImage.replace(" ", "%20"); // "\\s+" 
        UrlImageViewHelper.setUrlDrawable(mInstructorImage, IMG_URL + instructorImage, 
                R.drawable.image_placeholder);
        
        mInstructorBody.setText(Html.fromHtml(mDb.getCourseOutlineByTitle(sTitle, 
                InstructorColumns.INSTRUCTOR_BODY)));
    }
}
