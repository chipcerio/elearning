/*
 * Copyright (C) 2013 Alistair Cerio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eng.cucumber.elearning.app;

import static eng.cucumber.elearning.util.Cheese.PAGE1;
import static eng.cucumber.elearning.util.Cheese.PAGE2;
import static eng.cucumber.elearning.util.Cheese.PAGE3;

import java.util.ArrayList;
import java.util.List;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.view.ViewGroup;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.Tab;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;

import eng.cucumber.elearning.R;

public class MainActivity extends BaseActivity implements
        ViewPager.OnPageChangeListener, ActionBar.TabListener {
    
    private ViewPager mPager;
    private ActionBar mActionBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        setContentView(R.layout.a_main);
        
        // add fragments
        List<Fragment> fragments = new ArrayList<Fragment>();
        fragments.add(Fragment.instantiate(this, PAGE1));
        fragments.add(Fragment.instantiate(this, PAGE2));
        fragments.add(Fragment.instantiate(this, PAGE3));
        
        // adapter
        CategoryAdapter adapter = new CategoryAdapter(getSupportFragmentManager(), fragments);
        
        // pager
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setAdapter(adapter);
        mPager.setOnPageChangeListener(this);
        
        setupActionBarAndTabs();
    }
    
    private void setupActionBarAndTabs() {
        mActionBar = getSupportActionBar();
        
        mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        ActionBar.Tab courseTab = mActionBar.newTab().setText("COURSE");
        ActionBar.Tab universityTab = mActionBar.newTab().setText("UNIVERSITY");
        ActionBar.Tab lectureTab = mActionBar.newTab().setText("INSTRUCTOR");
        
        courseTab.setTabListener(this);
        universityTab.setTabListener(this);
        lectureTab.setTabListener(this);
        
        mActionBar.addTab(courseTab, true);
        mActionBar.addTab(universityTab);
        mActionBar.addTab(lectureTab);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.a_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return true;
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
        mActionBar.getTabAt(position).select();
    }

    @Override
    public void onTabSelected(Tab tab, FragmentTransaction ft) {
        mPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(Tab tab, FragmentTransaction ft) {
    }

    @Override
    public void onTabReselected(Tab tab, FragmentTransaction ft) {
    }
    
    public class CategoryAdapter extends FragmentPagerAdapter {
        List<Fragment> fragments;
        
        public CategoryAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
        }
    }

}
