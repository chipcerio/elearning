package eng.cucumber.elearning.app;

import static eng.cucumber.elearning.util.Cheese.IMG_URL;

import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import eng.cucumber.elearning.R;
import eng.cucumber.elearning.database.ELearningData;
import eng.cucumber.elearning.util.DebugLog;

public class SelectCategoryActivity extends SherlockActivity implements OnItemClickListener {
    private int mCategory;
    private String mTitle;
    private ELearningData mDb;
    private GridView mGrid;
    private List<String> mImageList;
    private List<String> mTitleList;
    
    private static final String TAG = "SelectCategoryActivity";
    
    @Override
    protected void onCreate(Bundle peanut) {
        super.onCreate(peanut);
        mDb = new ELearningData(this);
        setContentView(R.layout.a_select_category);
        
        // intent extras
        mCategory = getIntent().getIntExtra("category", 0);
        mTitle = getIntent().getStringExtra("title");
        
        // actionbar
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(mTitle);

        mImageList = mDb.getSubCategoryImages(mCategory);
        mTitleList = mDb.getSubCategoryTitles(mCategory);
        mGrid = (GridView) findViewById(R.id.grid_select_category);
        mGrid.setAdapter(new ImageAdapter(this));
        mGrid.setOnItemClickListener(this);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case android.R.id.home:
            finish();
            break;
        }
        
        return true;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        DebugLog.d(TAG, "title=" + mTitleList.get(position));
        
        Intent i = new Intent(this, MainActivity.class);
        i.putExtra("title", mTitleList.get(position));
        startActivity(i);
    }

    class ImageAdapter extends BaseAdapter {
        private int mSize;
        private Context mContext;
        private LayoutInflater mInflater;
        
        public ImageAdapter(Context context) {
            mSize = mTitleList.size();
            mContext = context;
            mInflater = LayoutInflater.from(mContext);
        }
        
        @Override
        public int getCount() {
            return mSize;
        }
    
        @Override
        public Object getItem(int position) {
            return null;
        }
    
        @Override
        public long getItemId(int position) {
            return 0;
        }
    
        @SuppressWarnings("static-access")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.r_item_subcategory, null);
                holder = new ViewHolder();
                holder.image = (ImageView) convertView.findViewById(R.id.img_grid_item);
                holder.title  = (TextView)  convertView.findViewById(R.id.txt_grid_caption);
                convertView.setTag(holder);
            
            } else {
                holder = (ViewHolder) convertView.getTag();
            } 
            
            String photo = mImageList.get(position);
            photo = photo.replaceAll("\\s+", "%20");
            UrlImageViewHelper.setUrlDrawable(holder.image, IMG_URL + photo, R.drawable.img_03_health);
            holder.title.setText(mTitleList.get(position));
            
            return convertView;
        }
    }
    
    static class ViewHolder {
        static ImageView image;
        static TextView title;
    }
    
    
}
