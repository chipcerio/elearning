/*
 * Copyright (C) 2013 Alistair Cerio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package eng.cucumber.elearning.app;

import static eng.cucumber.elearning.util.Cheese.IMG_URL;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockFragment;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import eng.cucumber.elearning.R;
import eng.cucumber.elearning.database.ELearningData;
import eng.cucumber.elearning.util.DBUtils.CourseColumns;
import eng.cucumber.elearning.util.DBUtils.InstructorColumns;

public class CourseDetailFragment extends SherlockFragment implements View.OnClickListener {
    private static int sIndex;
    private ELearningData mDb;
    
    private TextView mCourseTitle;
    private TextView mCourseDetail;
    private TextView mInstructorTitle;
    private TextView mInstructorDetail;
    private Button mReadMore;
    private ImageView mCourseImg;
    private ImageView mInstructorImg;
    
    public static CourseDetailFragment newInstance(int index) {
        CourseDetailFragment f = new CourseDetailFragment();
        sIndex = index + 1;
        
        Bundle args = new Bundle();
        args.putInt("index", index);
        f.setArguments(args);
        
        return f;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        
        // set course
        String photo = mDb.getCourseOutline(sIndex, CourseColumns.COURSE_IMAGE);
        photo = photo.replaceAll("\\s+", "%20");
        UrlImageViewHelper.setUrlDrawable(mCourseImg, IMG_URL + photo, R.drawable.image_placeholder);
        
        mCourseTitle.setText(mDb.getCourseOutline(sIndex, CourseColumns.COURSE_TITLE));
        mCourseDetail.setText(Html.fromHtml(mDb.getCourseOutline(sIndex, CourseColumns.COURSE_BODY)));
        
        // set instructor
        String photoInstr = mDb.getCourseOutline(sIndex, InstructorColumns.INSTRUCTOR_IMAGE);
        photoInstr = photoInstr.replaceAll("\\s+", "%20");
        UrlImageViewHelper.setUrlDrawable(mInstructorImg, IMG_URL + photoInstr, 
                R.drawable.image_placeholder);
        
        mInstructorTitle.setText(mDb.getCourseOutline(sIndex, InstructorColumns.INSTRUCTOR_TITLE));
        mInstructorDetail.setText(Html.fromHtml(mDb.getCourseOutline(sIndex, 
                InstructorColumns.INSTRUCTOR_BODY)));
        
        // set button if TED Talks selected
        String id = mDb.getCourseOutline(sIndex, CourseColumns.COURSE_ID);
        if (id.contentEquals("22")) { // course_id 22
            mReadMore.setVisibility(View.VISIBLE);
            mReadMore.setText("Read More");
            mReadMore.setOnClickListener(this);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle icicle) {
        View root = inflater.inflate(R.layout.f_course_detail, container, false);
        mDb = ((BaseActivity) getActivity()).mAppDb;
        
        mCourseImg = (ImageView) root.findViewById(R.id.img_course);
        mCourseTitle = (TextView) root.findViewById(R.id.text_course_title);
        mCourseDetail = (TextView) root.findViewById(R.id.text_course_detail);
        
        mReadMore = (Button) root.findViewById(R.id.btn_course_read_more);
        
        mInstructorImg = (ImageView) root.findViewById(R.id.img_course_instructor);
        mInstructorTitle = (TextView) root.findViewById(R.id.text_course_instructor_title);
        mInstructorDetail = (TextView) root.findViewById(R.id.text_course_instructor_detail);
        
        return root;
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent(getActivity(), TedTalksActivity.class);
        startActivity(i);
    }

}
