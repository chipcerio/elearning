package eng.cucumber.elearning.app;
/*
 * Copyright (C) 2013 Alistair Cerio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static eng.cucumber.elearning.util.Cheese.BASE_URL;
import static eng.cucumber.elearning.util.Cheese.IMG_URL;
import static eng.cucumber.elearning.util.Cheese.VIDEO_DIR;

import java.io.File;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DownloadManager;
import android.app.DownloadManager.Query;
import android.app.DownloadManager.Request;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.text.Html;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.app.SherlockDialogFragment;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuInflater;
import com.actionbarsherlock.view.MenuItem;
import com.koushikdutta.urlimageviewhelper.UrlImageViewHelper;

import eng.cucumber.elearning.R;
import eng.cucumber.elearning.database.ELearningData;
import eng.cucumber.elearning.util.DebugLog;
import eng.cucumber.elearning.util.ELearningUtil;
import eng.cucumber.elearning.util.NetworkUtils;
import eng.cucumber.elearning.util.UIUtils;

public class VideoInfoActivity extends SherlockFragmentActivity implements View.OnClickListener {
    private int mSelected;
    private int mVideoType;
    private ImageView mImage;
    private TextView mTitle;
    private TextView mBody;
    private ELearningData mDb;
    private String mDownloadDir;
    private BroadcastReceiver mBroadcastReceiver;

    private String mInstructor;
    private String mDescription;
    private String mVideoUrl;
    private String mVideoLocal;
    private String mPhoto;
    private String mFileName;

    private long mEnqueue;
    private DownloadManager mDownload;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.a_video_info);
        mDb = new ELearningData(this);
        mSelected = getIntent().getIntExtra("position", 0);
        mDownloadDir = getIntent().getStringExtra("type");
        mVideoType = getIntent().getIntExtra("video", 0);

        mImage = (ImageView) findViewById(R.id.img_video_info_image);
        mTitle = (TextView) findViewById(R.id.txt_video_info_title);
        mBody = (TextView) findViewById(R.id.txt_video_info_body);

        switch (mVideoType) {
            case ELearningUtil.TED_VIDEO:
                mInstructor = mDb.getTedTalksTitle(mSelected);
                mDescription = mDb.getTedTalksDetails(mSelected);
                mVideoUrl = BASE_URL + VIDEO_DIR + mDb.getTedSdVideoUrl(mSelected)
                        .replaceAll("\\s+", "%20");

                mVideoLocal = "";
                String[] dir = mDb.getTedSdVideoForLocal(mSelected).split("/");
                for (int i = 0; i < dir.length; i++) {
                    if (!dir[i].contains(".")) {
                        mVideoLocal = mVideoLocal + dir[i] + "/";
                    } else {
                        mFileName = dir[i];
                    }
                }

                mPhoto = mDb.getTedTalksImage(mSelected);
                
                Toast.makeText(this, "video_url=" + mVideoUrl, Toast.LENGTH_LONG).show();
                break;

            case ELearningUtil.ECORNER_VIDEO:
                mInstructor = mDb.getECornerTitle(mSelected);
                mDescription = mDb.getECornerDetails(mSelected);
                mVideoUrl = mDb.getECornerSdVideoUrl(mSelected)
                        .replaceAll("\\s+", "%20");
                
                mVideoLocal = "";
                String[] ls = mDb.getECornerSdVideoForLocal(mSelected).split("/");
                for (int j = 0; j < ls.length; j++) {
                    if (!ls[j].contains(".")) {
                        mVideoLocal = mVideoLocal + ls[j].replaceAll("\\s+", "") + "/";
                    } else {
                        mFileName = ls[j].replaceAll("\\s+", "");
                    }
                }
                
                mPhoto = mDb.getECornerImage(mSelected);
                
                Toast.makeText(this, "video_url=" + mVideoUrl, Toast.LENGTH_LONG).show();
                break;

            default:
                break;
        }

        displayImage(mSelected);
        mTitle.setText(mInstructor);
        mBody.setText(Html.fromHtml(mDescription));

        mBroadcastReceiver = getBroadcastReceiver();
        mImage.setOnClickListener(this);
        registerReceiver(mBroadcastReceiver,
                new IntentFilter(DownloadManager.ACTION_DOWNLOAD_COMPLETE));

        File file = new File(Environment.getExternalStorageDirectory()
                .getAbsolutePath(), mDownloadDir + mVideoLocal);
        if (!file.exists()) {
            file.mkdirs();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(mBroadcastReceiver);
    }

    private void displayImage(int position) {
        mPhoto = mPhoto.replaceAll("\\s+", "%20");
        UrlImageViewHelper.setUrlDrawable(mImage, IMG_URL + mPhoto, R.drawable.image_placeholder);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private BroadcastReceiver getBroadcastReceiver() {
        BroadcastReceiver receiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                String action = intent.getAction();
                if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                    long downloadId = intent.getLongExtra(DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                    Query q = new Query();
                    q.setFilterById(mEnqueue);
                    Cursor c = mDownload.query(q);
                    if (c.moveToFirst()) {
                        int columnIndex = c.getColumnIndex(DownloadManager.COLUMN_STATUS);

                        if (DownloadManager.STATUS_SUCCESSFUL == c.getInt(columnIndex)) {
                            if (UIUtils.hasHoneycomb()) {
                                DebugLog.d("Downloaded >>>>>>>>", "path=" + mDownload
                                        .getUriForDownloadedFile(downloadId));
                            }
                            Intent i = new Intent(VideoInfoActivity.this, VideoActivity.class);
                            i.putExtra("selected", mSelected);
                            i.putExtra("type", mDownloadDir);
                            startActivity(i);
                            finish();
                        }

                    }
                }
            }
        };

        return receiver;
    }

    @Override
    public void onClick(View v) {
        if (NetworkUtils.isNetworkAvailable(this)) {
            Intent i = new Intent(this, VideoActivity.class);
            i.putExtra("selected", mSelected);
            i.putExtra("type", mDownloadDir);
            i.putExtra("video", mVideoType);
            startActivity(i);
            finish();

        } else {
            showDialog();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getSupportMenuInflater();
        inflater.inflate(R.menu.a_video_info, menu);
        return true;
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_download:
                if (NetworkUtils.isNetworkAvailable(this)) {
                    mDownload = (DownloadManager) getSystemService(DOWNLOAD_SERVICE);
                    Request request = new Request(Uri.parse(mVideoUrl));

                    if (UIUtils.hasHoneycomb()) {
                        request.setNotificationVisibility(DownloadManager.Request
                                .VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
                    }

                    request.setDestinationInExternalPublicDir(mDownloadDir, mVideoLocal + mFileName);
                    mEnqueue = mDownload.enqueue(request);

                } else {
                    showDialog();
                }
                break;

            default:
                break;
        }

        return true;
    }

    void showDialog() {
        DialogFragment newFragment = NoInternetDialog.newInstance(R.string.alert_dialog_title);
        newFragment.show(getSupportFragmentManager(), "dialog");
    }

    public void doPositiveClick() {
        Intent i = new Intent(this, VideoActivity.class);
        i.putExtra("selected", mSelected);
        i.putExtra("type", mDownloadDir);
        i.putExtra("video", mVideoType);
        startActivity(i);
        finish();
    }

    public static class NoInternetDialog extends SherlockDialogFragment {
        public static NoInternetDialog newInstance(int title) {
            NoInternetDialog fragment = new NoInternetDialog();
            Bundle args = new Bundle();
            args.putInt("title", title);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            int title = getArguments().getInt("title");

            return new AlertDialog.Builder(getActivity())
            .setIcon(R.drawable.ic_launcher)
            .setTitle(title)
            .setMessage(R.string.alert_dialog_message)
            .setPositiveButton(R.string.alert_dialog_ok,
                    new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    ((VideoInfoActivity) getActivity()).doPositiveClick();
                }
            }).create();
        }
    }
}
