package eng.cucumber.elearning.app;
/*
 * Copyright (C) 2013 Alistair Cerio
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import static eng.cucumber.elearning.util.Cheese.HAS_UPDATE_URL;
import static eng.cucumber.elearning.util.Cheese.FORMAT_JSON;

import net.simonvt.menudrawer.MenuDrawer;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;

import eng.cucumber.elearning.R;
import eng.cucumber.elearning.database.ELearningData;
import eng.cucumber.elearning.os.UpdateTask;
import eng.cucumber.elearning.util.DebugLog;
import eng.cucumber.elearning.util.ELearningUtil;

public class MainActivity2 extends SherlockFragmentActivity implements OnClickListener, 
OnNavigationListener {
    
    protected ELearningData mDb;
    private boolean mHasUser;
    private static final int ACTION_LOGIN   = 1;
    private static final int ACTION_ABOUT   = 2;
    private static final int ACTION_LOGOUT  = 3;
    private static final int ACTION_REFRESH = 4;
    private static Menu sOptionsMenu;
    private ActionBar mActionBar;
    private MenuDrawer mMenuDrawer;

    public static boolean sIsUpdating;

    @Override
    protected void onCreate(Bundle cucumber) {
        super.onCreate(cucumber);
        mDb = new ELearningData(this);
        mHasUser = ELearningApp.getInstance().hasUser();
        mActionBar = getSupportActionBar();

        mActionBar.setDisplayHomeAsUpEnabled(true);
        
        // menu drawer
        mMenuDrawer = MenuDrawer.attach(this, MenuDrawer.MENU_DRAG_WINDOW);
        mMenuDrawer.setContentView(R.layout.a_main2);
        if (mHasUser) { // attach menu drawer
            mMenuDrawer.setMenuView(R.layout.menu_scrollview);
            mMenuDrawer.setMenuSize(400);
            mMenuDrawer.setTouchMode(MenuDrawer.TOUCH_MODE_FULLSCREEN);
            mMenuDrawer.peekDrawer(1000,5000);
            setListeners();
        } else { // default
            setContentView(R.layout.a_main2);
            mMenuDrawer.setMenuSize(0);
        }
        
        home();
        mTedGroup = getResources().getStringArray(R.array.ted_videos);
        mStanfordGroup = getResources().getStringArray(R.array.ecorner_videos);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        sOptionsMenu = menu;
        
        if (mHasUser) {
            menu.add(0, ACTION_LOGOUT, 0, "Logout")
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
            
        } else {
            menu.add(0, ACTION_LOGIN, 0, "Login")
                .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        }
        
        menu.add(0, ACTION_REFRESH, 0, "Update")
            .setIcon(R.drawable.ic_action_refresh)
            .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        
        menu.add(0, ACTION_ABOUT, 0, "About")
            .setIcon(R.drawable.ic_action_about)
            .setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mHasUser) {
            switch (item.getItemId()) {
            case ACTION_ABOUT:
                Intent j = new Intent(this, MainActivity.class);
                startActivity(j);
                finish();
                break;
                
            case ACTION_LOGOUT:
                ELearningApp.getInstance().setUserFlag(false);
                invalidateOptionsMenu();
                
                Intent k = new Intent(this, MainActivity2.class);
                startActivity(k);
                finish();
                break;
                
            case ACTION_REFRESH:
                fetchUpdates();
                setRefreshActionButtonState(true);
                break;
                
            case android.R.id.home:
                home();
                mActionBar.setTitle("eLearning");
                break;
            }
            
        } else {
            switch (item.getItemId()) {
            case ACTION_LOGIN:
                Intent i = new Intent(this, LoginActivity.class);
                startActivity(i);
                finish();
                break;
                
            case ACTION_ABOUT:
                Intent j = new Intent(this, MainActivity.class);
                startActivity(j);
                finish();
                break;
                
            case android.R.id.home:
                home();
                break;
            }
            
        }
        
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
        case R.id.txt_menu_software:
            displayCourseOutline(getString(R.string.menudrawer_saas));
            break;
            
        case R.id.txt_menu_computer:
            displayCourseOutline(getString(R.string.menudrawer_comp_vision));
            break;
            
        case R.id.txt_menu_gmat:
            displayCourseOutline(getString(R.string.menudrawer_gmat));
            break;
            
        case R.id.txt_menu_ted:
            displayCourseOutline(getString(R.string.menudrawer_ted));
            break;
            
        case R.id.txt_menu_sub_ted:
            displayCourseGroup("TED Talks Group");
            displayTedListNavigation(true);
            break;
            
        case R.id.txt_menu_ecorner:
            displayCourseOutline(getString(R.string.menudrawer_ecorner));
            break;
            
        case R.id.txt_menu_sub_ecorner:
            displayCourseGroup("Ecorner Group");
            displayStanfordListNavigation(true);
            break;
            
        default:
            break;
        }
    }
    
    public static void setRefreshActionButtonState(boolean updating) {
        if (sOptionsMenu == null) {
            return;
        }
        
        final MenuItem refreshItem = sOptionsMenu.findItem(ACTION_REFRESH);
        if (refreshItem != null) {
            if (updating) {
                refreshItem.setActionView(R.layout.actionbar_indeterminate_progress);
            } else {
                refreshItem.setActionView(null);
            }
            sIsUpdating = updating;
        }
    }
    
    private void fetchUpdates() {
        String lastModified = mDb.getLastModifiedDate();
        String current = ELearningUtil.getCurrentDate();
        String dateRange = lastModified + "--" + current;
        
        String url = HAS_UPDATE_URL + dateRange + FORMAT_JSON;
        new UpdateTask(this, mDb, dateRange).execute(url);
    }
    
    private void home() {
        Fragment fragment = new CategoryFragment();
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container_fragment_details, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(null);
        ft.commit();
        
        displayTedListNavigation(false);
        displayStanfordListNavigation(false);
    }

    private void setListeners() {
        findViewById(R.id.txt_menu_software).setOnClickListener(this);
        findViewById(R.id.txt_menu_sub_sas).setOnClickListener(this);
        findViewById(R.id.txt_menu_computer).setOnClickListener(this);
        findViewById(R.id.txt_menu_sub_computer).setOnClickListener(this);
        findViewById(R.id.txt_menu_gmat).setOnClickListener(this);
        findViewById(R.id.txt_menu_sub_gmat).setOnClickListener(this);
        findViewById(R.id.txt_menu_engvid).setOnClickListener(this);
        findViewById(R.id.txt_menu_sub_ronnie).setOnClickListener(this);
        findViewById(R.id.txt_menu_sub_alex).setOnClickListener(this);
        findViewById(R.id.txt_menu_sub_emma).setOnClickListener(this);
        findViewById(R.id.txt_menu_sub_valen).setOnClickListener(this);
        findViewById(R.id.txt_menu_ted).setOnClickListener(this);
        findViewById(R.id.txt_menu_sub_ted).setOnClickListener(this);
        findViewById(R.id.txt_menu_ecorner).setOnClickListener(this);
        findViewById(R.id.txt_menu_sub_ecorner).setOnClickListener(this);
    }

    private void displayCourseOutline(String title) {
        Fragment fragment = CourseOutlineFragment.newInstance(title);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container_fragment_details, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(null);
        ft.commit();
        
        mMenuDrawer.closeMenu();
        mActionBar.setTitle(title);
        displayTedListNavigation(false);
        displayStanfordListNavigation(false);
    }
    
    private void displayCourseGroup(String title) {
        Fragment fragment = CourseGroupFragment.newInstance(title);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container_fragment_details, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(null);
        ft.commit();
        
        mMenuDrawer.closeMenu();
        mActionBar.setTitle(title);
    }
    
    private void displayStanfordListNavigation(boolean isStanford) {
        Context context = mActionBar.getThemedContext();
        ArrayAdapter<CharSequence> list = ArrayAdapter.createFromResource(context, 
                R.array.ecorner_videos, com.actionbarsherlock.R.layout.sherlock_spinner_item);
        list.setDropDownViewResource(com.actionbarsherlock.R.layout.sherlock_spinner_dropdown_item);
        
        if (isStanford) {
            mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
            mActionBar.setListNavigationCallbacks(list, this);
        } else {
            mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        }
    }
    
    private void displayTedListNavigation(boolean isTed) {
        Context context = mActionBar.getThemedContext();
        ArrayAdapter<CharSequence> list = ArrayAdapter.createFromResource(context, 
                R.array.ted_videos, com.actionbarsherlock.R.layout.sherlock_spinner_item);
        list.setDropDownViewResource(com.actionbarsherlock.R.layout.sherlock_spinner_dropdown_item);
        
        if (isTed) {
            mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
            mActionBar.setListNavigationCallbacks(list, this);
        } else {
            mActionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        }
        
        mMenuDrawer.closeMenu();
    }
    
    private String[] mTedGroup, mStanfordGroup;
    @SuppressLint("DefaultLocale")
    @Override
    public boolean onNavigationItemSelected(int itemPosition, long itemId) {
        
        if (mActionBar.getTitle().toString().contentEquals("TED Talks Group")) {
            String group = mTedGroup[itemPosition].toLowerCase();
            
            DebugLog.d(MainActivity2.class.getName(), "group=" + group);
            
            Fragment fragment = TedTalksGroupFragment.newInstance(group);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.container_fragment_details, fragment);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(null);
            ft.commit();
        
        } else {
            String group = mStanfordGroup[itemPosition];
            
            Fragment fragment = StanfordGroupFragment.newInstance(group);
            FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.container_fragment_details, fragment);
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(null);
            ft.commit();
        }
        
        
        return true;
    }
}
