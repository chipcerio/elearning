package eng.cucumber.elearning.app;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;

import eng.cucumber.elearning.R;
import eng.cucumber.elearning.database.ELearningData;
import eng.cucumber.elearning.util.Cheese;

public class CategoryFragment extends SherlockFragment implements OnItemClickListener {
    private GridView mGrid;
    private ELearningData mDb;
    private ActionBar mActionBar;
    
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle cucumber) {
        View root = inflater.inflate(R.layout.f_category, container, false);
     
        mGrid = (GridView) root.findViewById(R.id.grid_main_cat);
        mDb = ((MainActivity2) getActivity()).mDb;
        mActionBar = getSherlockActivity().getSupportActionBar();
        
        return root;
    }
    
    @Override
    public void onActivityCreated(Bundle cucumber) {
        super.onActivityCreated(cucumber);
        mGrid.setAdapter(new ImageAdapter(getActivity()));
        mGrid.setOnItemClickListener(this);
    }
    
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String title = mDb.hasDb()
                     ? mDb.getCategories().get(position)
                     : Cheese.CATEGORIES[position];
        mActionBar.setTitle(title);
        replaceFragment(++position);
    }

    private void replaceFragment(int position) {
        Fragment fragment = SelectCourseFragment.newInstance(position);
        FragmentTransaction ft = getSherlockActivity().getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container_fragment_details, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
        ft.addToBackStack(null);
        ft.commit();
    }
    
    class ImageAdapter extends BaseAdapter {
        private Context mContext;
        private LayoutInflater mInflater;
        
        public ImageAdapter(Context context) {
            mContext = context;
            mInflater = LayoutInflater.from(mContext);
        }

        @Override
        public int getCount() {
            if (mDb.hasDb()) {
                return mDb.getCategories().size();
            } else {
                return Cheese.IMAGES.length;
            }
        }

        @Override
        public Object getItem(int arg0) {
            return null;
        }

        @Override
        public long getItemId(int arg0) {
            return 0;
        }

        @SuppressWarnings("static-access")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder;
            
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.r_item_category, null);
                holder = new ViewHolder();
                
                holder.image = (ImageView) convertView.findViewById(R.id.img_grid_item);
                holder.text  = (TextView)  convertView.findViewById(R.id.txt_grid_caption);
                convertView.setTag(holder);
                
            } else {
                holder = (ViewHolder) convertView.getTag();
            }
            
            /*
             * at first launch, app crashes resulting to NullPointerException. It is because database
             * copy operation hasn't finished yet. This code snippet uses static data at first launch
             * then uses the database afterwards.
             */
            if (mDb.hasDb()) {
                holder.image.setImageResource(Cheese.IMAGES[position]);
                holder.text.setText(mDb.getCategories().get(position));
            } else {
                holder.image.setImageResource(Cheese.IMAGES[position]);
                holder.text.setText(Cheese.CATEGORIES[position]);
            }
            
            return convertView;
        }
        
    }
    
    static class ViewHolder {
        static ImageView image;
        static TextView text;
    }
}
